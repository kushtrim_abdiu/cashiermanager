var app = angular.module('EliteApps', ['ui.router','ngMaterial','ngMdIcons'])
.config(function($urlRouterProvider,$stateProvider,$mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('deep-purple')
    .accentPalette('teal');

  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('app', {
        url:'/',
        templateUrl:'templates/app.html',
        controller: 'appController'
    })

});

app.controller('EliteAppsCtrl', ['$scope','$rootScope','$location', function($scope,$rootScope,$location) {

}]);
