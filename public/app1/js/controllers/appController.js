angular.module('EliteApps')
.controller('appController', ['$scope','$rootScope','$location','$mdBottomSheet',
            '$mdDialog','$mdToast','$rootElement','$mdSidenav','BusinessData',
function($scope,$rootScope,$location,$mdBottomSheet,$mdDialog,$mdToast,$rootElement,$mdSidenav,BusinessData) {
    //global variables
    $scope.DATE = new Date();
    $rootScope.COMPANY_NAME = 'EliteCaffe';
    $rootScope.IS_LOGGED_IN = false;
    $rootScope.LOCAL_TEXTS;
    $rootScope.ITEMS;
    $rootScope.TABLES;
    $rootScope.SUB_CATEGORIES = [];
    $rootScope.WAITERS;
    ////app variables ////////
    $rootScope.selected_table = 0;
    $rootScope.current_waiter = '';
    $rootScope.current_ticket = [];
    $rootScope.has_tables = false;
    $rootScope.category_is_selected = false;
    $rootScope.side_menu_is_selected = false;
    $scope.editable = true;
    /////// ////////////// /////////
    //on page load
    $scope.$on('$viewContentLoaded', function() {
        //$scope.authenticateBusiness({"username":"kushtrim","password":"123456"});
        $scope.authenticateBusiness();
        $scope.initiateBusiness();
    	//show waiter login
        if ($rootScope.current_waiter == '') {
            //no one is identified so show the login form
            $scope.login();
        }
	});

    $scope.initiateBusiness = function() {
        BusinessData.authenticate({"username":'elitebar',"password":'123456'}).done(function(data){
            console.log(data);
        });
        /*
        BusinessData.getBusinessData().done(function(data){
            $rootScope.WAITERS = data[0].waiters;
            $rootScope.TABLES = data[0].tables;
            $rootScope.LOCAL_TEXTS = data[0].strings[0];
            $rootScope.ITEMS = data[0].items;
        }); */
    };

    $scope.authenticateBusiness = function() {
        //BusinessData.authenticateBusiness();
    };

    $scope.selectCategory = function(category) {
        //if a table is selected then show the subcategories otherwise show message table not selected
        if ($rootScope.selected_table != 0) {
            $rootScope.category_is_selected = true;
            $rootScope.SUB_CATEGORIES = [];
            for (var i=0; i<$rootScope.ITEMS.length; i++) {
                if ($rootScope.ITEMS[i].category == category) {
                    if ($rootScope.SUB_CATEGORIES.indexOf($rootScope.ITEMS[i].sub_category) == -1) {
                        $rootScope.SUB_CATEGORIES.push($rootScope.ITEMS[i].sub_category);
                    }
                }
            }
            $scope.sub_categories = $rootScope.SUB_CATEGORIES;
            //console.log($rootScope.SUB_CATEGORIES);
        }
        else {
            //show the message no table is selected
            $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_table_selected_msg));
        }
    }

    //waiter identification
    $scope.login = function($scope) {
		$mdDialog.show({
	      controller: loginController,
	      templateUrl: 'views/log-in.html',
	    })

	    function loginController($scope) {
            $scope.login_label = $rootScope.LOCAL_TEXTS.login_label;
	    	$scope.user_input = "";

            //login button is clicked
	    	$scope.logMeIn = function() {
	           //check if we have any pin matching the user input
                $scope.waiters = $rootScope.WAITERS;
                var new_waiter = sucessfullAuth($scope.user_input);
                if (new_waiter != '') {
                    $rootScope.current_waiter = new_waiter;
	    			$mdDialog.cancel();
                }
                else {
                    //the password was wrong
                    $scope.user_input = '';
                }
	    	};
	    	//when users click on the button
	    	$scope.inputClicked = function(nr) {
	    		//if backspace is not clicked
	    		if (nr!=-1) {
	    			$scope.user_input += nr;
	    		}
	    		else {
                    var new_input="";
                    for (var i=0; i<$scope.user_input.length-1; i++) {
                        new_input += $scope.user_input[i];
                    }
	    			$scope.user_input = new_input;
	    		}
	    	}
	    }

	};

    //logout
    $scope.logOut = function() {
        for (var i=0; i<$rootScope.WAITERS.length; i++) {
            if ($rootScope.WAITERS[i].logged_in) {
                $rootScope.WAITERS[i].logged_in = false;
                //deselect the last selected ticket
                //$rootScope.current_ticket = [];
                $rootScope.selected_table = 0;
                $rootScope.sub_categories = [];
                //$rootScope.current_sub_category;
                //$scope.editable = true;
                $rootScope.category_is_selected = false;
                $rootScope.current_waiter = '';
                $rootScope.has_tables = false;
                $scope.login();
                break;
            }
        }
    }

    //button add table is clicked
	$scope.addTable = function(event) {
		//show the popup with tables
		$mdDialog.show({
	      controller: addTableController,
	      templateUrl: 'views/choose-table.html',
	    })
	    function addTableController($scope) {
            $scope.choose_table_label = $rootScope.LOCAL_TEXTS.choose_table_label;
            $scope.all_tables = $rootScope.TABLES;
            //console.log($scope.all_tables);
			$scope.addButton = function(button_nr) {
                for (var i=0; i<$rootScope.TABLES.length; i++) {
                    if ($rootScope.TABLES[i].nr == button_nr) {
                        $rootScope.TABLES[i].waiter = $rootScope.current_waiter;
                        $rootScope.has_tables = true;
                        $mdDialog.cancel();
                        break;
                    }
                }
			}
			$scope.tableDialogExit = function(event) {
                $mdDialog.cancel();
			}
		}
	}

    //select a table that is added
    $scope.selectTable = function (table_nr) {
        $rootScope.selected_table = table_nr;
        $rootScope.category_is_selected = false;
        $scope.SUB_CATEGORIES = [];
    }

    $scope.addItemToTicket = function(item_name,item_price) {
        //first we have to check if the ticket is empty
		var new_item = {name:item_name,amount:1,price:item_price,checked:true};
		var new_ticket = [];
		//console.log(new_item);
		var flag = false ;
		for (var i=0;i<$rootScope.TABLES.length;i++) {
			if ($rootScope.TABLES[i].nr == $rootScope.selected_table) {
				//mark the selected ticket
				$rootScope.current_ticket = $rootScope.TABLES[i];
				//we have to prepare the array that we will insert into the tickets of the currently selected table
				//if the length is zero it means there is no item in the ticket
				if ($rootScope.TABLES[i].ticket.length!=0) {
				 	for (var j=0;j<$rootScope.TABLES[i].ticket.length;j++) {
			 			//check if the item already exists in the ticket
			 			if (item_exists_in_ticket($rootScope.TABLES[i].ticket[j].name,item_name)) {
			 				flag = true;
			 				break;
			 			}
				 	}
				 	if (flag) {
						var amount = $rootScope.TABLES[i].ticket[j].amount;
		 				$rootScope.TABLES[i].ticket[j].amount = amount + 1;
		 				//console.log($rootScope.TABLES[i].ticket[j].amount);
		 				flag = false;
					}
					else {
						$rootScope.TABLES[i].ticket.push(new_item);
				 		//console.log($rootScope.TABLES[i].ticket);
					}
				}
				else {
					//the ticket is empty so we dont need to check if the item exists or not we should just add it
					//$rootScope.all_tables[i].ticket.push(new_item);
					new_ticket.push(new_item);
					$rootScope.TABLES[i].ticket = new_ticket;
					//console.log($rootScope.TABLES[i].ticket);
				}
				//update the price of the ticket
				$rootScope.TABLES[i].total += item_price;
			}
		}
    };

    $scope.removeItem = function(name,price) {
        for (var i=0;i<$scope.TABLES.length; i++) {
    		if ($rootScope.TABLES[i].nr == $rootScope.selected_table) {
    			for(var j=0; j<$rootScope.TABLES[i].ticket.length;j++) {
    				if ($rootScope.TABLES[i].ticket[j].name == name) {
    					//now if the amount is one then remove all the item from the list
    					if ($rootScope.TABLES[i].ticket[j].amount ==1) {
    						$rootScope.TABLES[i].ticket.splice(j, 1);
    					}
    					else {
    						//decrease the amount by 1
    						$rootScope.TABLES[i].ticket[j].amount --;
    					}
    					//update the price too
    					$rootScope.TABLES[i].total -= price;
    					break;
    				}
    			}
    		}
    	}
    };

    $scope.openWorkerSideNav = function(event) {
        $rootScope.side_menu_is_selected = true;
	    $mdSidenav('worker_sidenav').toggle();
	};

    //print the selected ticked with the items that are checked
    $scope.printTicket = function() {

    };

    //pay the ticked and free the table
    $scope.payTicket = function() {
        //show the popup with tables
        if ($rootScope.selected_table != 0) {
            $mdDialog.show({
              controller: 'paymentController',
              templateUrl: 'views/add-payment.html'
            })
        }
        else {
            alert('not set');
        }
    }
    ///////// HELPER FUNCTIONS /////////////////////////////
    function sucessfullAuth(user_input) {
        for (var i=0; i<$rootScope.WAITERS.length; i++) {
            if ($rootScope.WAITERS[i].password == user_input) {
                $rootScope.WAITERS[i].logged_in = true;
                return $rootScope.WAITERS[i].name;
            }
        }

        return '';
    }

    function item_exists_in_ticket(item_name_in_ticket,item_name) {
		var item_in_ticket = item_name_in_ticket.replace(" ","");
		var item = item_name.replace(" ","");
		//console.log(item_in_ticket.toLowerCase()+' : '+item.toLowerCase());
		if (item_in_ticket.toLowerCase() == item.toLowerCase()) {
			return true;
		}
		else {
			false;
		}
	}
    /////////////////////////////////////////////////////////
}])
//the directive of sub categories where we have the tabs
.directive('subCategories', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/sub-categories.html',
    	controller: 'appController'
 	};
})
// the directive of side-menu with waiter info
.directive('sideMenu', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/side-menu.html',
    	controller: 'appController'
 	};
})
//API RESULTS
//the app texts for translation purposes
var texts = {
        'no_tables_msg' : 'NUK KENI TAVOLINE',
        'food_label' : 'USHQIM',
        'drinks_label' : 'PIJE',
        'snacks_label' : 'SNACKS',
        'other_label' : 'TJERA',
        'no_category_selected_msg' : 'NUK KENI ZGJEDHUR KATEGORI',
        'choose_table_label' : 'ZGJIDH TAVOLINE',
        'login_label' : 'LOGOHU',
        'no_table_selected_msg' : 'NUK KENI ZGJEDHUR ASNJE TAVOLINE',
        'pay' : 'PAGO',
        'print' : 'PRINTO',
        'ticket' : 'TIKETA',
        'selected_table_label' : 'TAVOLINA:',
        'waiter_info_label' : 'INFORMATA PER KAMARIERIN',
        'tables_served_label' : 'tavolina te servuara',
        'waiter_action_label' : 'OPCIONE',
        'change_ping_label' : 'NDRYSHO PININ',
        'create_reservation_label' : 'KRIJO REZERVIM',
        'add_new_product_label' : 'SHTO PRODUKT TE RI',
        'punch_out_label' : 'MBYLL',
        'register_payment_label' : 'PAGO',
        'enter_client_payment_label' : 'HYRJE',
        'return_amount_label' : 'KTHE KLIENTIT',

   };

var items = [
		{name:'Coke', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Pepsi', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Fanta', category:'drink', sub_category:'non-alcoholic', price:2},
		{name:'Sprite', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Mineral Water', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Apple Juice', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Orange Juice', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Lemonade', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Machiato', category:'drink', sub_category:'coffee', price:2},
		{name:'Latte', category:'drink', sub_category:'coffee', price:2},
		{name:'Black Coffee', category:'drink', sub_category:'coffee', price:2},
		{name:'Capuchinno', category:'drink', sub_category:'coffee', price:2},
		{name:'Nescaffe', category:'drink', sub_category:'coffee', price:2},
		{name:'Apple Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Fruit Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Grapes Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Chicken Burger', category:'food', sub_category:'fast-food', price:22},
		{name:'Hamburger', category:'food', sub_category:'fast-food', price:22},
		{name:'Chicken Cheese', category:'food', sub_category:'fast-food', price:22},
		{name:'French Fries', category:'food', sub_category:'fast-food', price:22}
	];

var tables = [
        {nr:'1',available:true,waiter:"free", ticket:[],total:0},
        {nr:'2',available:true,waiter:"free", ticket:[],total:0},
        {nr:'3',available:true,waiter:"free", ticket:[],total:0},
        {nr:'4',available:true,waiter:"free", ticket:[],total:0},
        {nr:'5',available:true,waiter:"free", ticket:[],total:0},
        {nr:'6',available:true,waiter:"free", ticket:[],total:0},
        {nr:'7',available:true,waiter:"free", ticket:[],total:0},
        {nr:'8',available:true,waiter:"free", ticket:[],total:0},
        {nr:'9',available:true,waiter:"free", ticket:[],total:0},
        {nr:'10',available:true,waiter:"free", ticket:[],total:0},
        {nr:'11',available:true,waiter:"free", ticket:[],total:0},
        {nr:'12',available:true,waiter:"free", ticket:[],total:0}
     ];

var waiters = [
        {name:'Kushtrim',password:'123',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Kreshnik',password:'456',logged_in:false,position:'Manager',profit:0,tables_served:0},
        {name:'Patriot',password:'789',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Nazif',password:'159',logged_in:false,position:'Waiter',profit:0,tables_served:0},
    ];
