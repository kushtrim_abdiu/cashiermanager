angular.module('EliteApps')
.controller('paymentController', ['$scope','$rootScope','$location','$mdBottomSheet','$mdDialog','$mdToast','$rootElement','$mdSidenav',
function($scope,$rootScope,$location,$mdBottomSheet,$mdDialog,$mdToast,$rootElement,$mdSidenav) {
      //button add table is clicked 
    $scope.returnAmountBool = false;
    $scope.showKeyboard = false;
    $scope.waiter_input ='';
    $scope.return_amount;
    $scope.strings = $rootScope.LOCAL_TEXTS;
    
    $scope.tableDialogExit = function(event) {
        $rootScope.current_ticket = [];
        //we have to release the table and reset its ticket
        alert('removing...');
        $scope.releaseTable();
        $mdDialog.cancel();
    }

    $scope.toggleKeyboard = function() {
        $scope.showKeyboard = !$scope.showKeyboard;
    }

    //when enter is pressed 
    $scope.enterPressed = function(ev) {
        if (ev.which === 13) {
            //enter pressed
            $scope.calculateReturn();
        }
    }

    //when users click on the button
    $scope.inputClicked = function(nr){
        //if backspace is not clicked
        if (nr!=-1) {
            $scope.waiter_input +=nr;
        }
        else {
            $scope.waiter_input = removeLastDigit($scope.waiter_input);
        }
    }

    $scope.calculateReturn = function() {
        //calculate returning amount 
        $scope.return_amount = calculateTheTicketPrice($scope.waiter_input);
        if ($scope.return_amount>0) {
            //show the return amount 
            $scope.returnAmountBool = true;
        }
        else {
            
        }
    }

    //release table
    $scope.releaseTable = function() {
        for (var i=0;i<$rootScope.TABLES.length;i++) {
            if ($rootScope.TABLES[i].nr == $rootScope.selected_table) {
                $rootScope.TABLES[i].waiter = 'free';
                $rootScope.TABLES[i].ticket = [];
                $rootScope.TABLES[i].total = 0;
                $rootScope.selected_table = 0;
                $rootScope.has_tables = false;
                $rootScope.category_is_selected = false;
                break;
            }
        }
    }

	//function which removes the last digit from the waiter input 
	function removeLastDigit(input) {
	 	var new_input="";
	 	for (var i=0; i<input.length-1; i++) {
	 		new_input += input[i];
	 	}

	 	return new_input;
	}

	function calculateTheTicketPrice(waiter_input) {
		var sum = 0;
		for (var i=0; i<$rootScope.current_ticket.ticket.length;i++) {
			sum += ($rootScope.current_ticket.ticket[i].price *$rootScope.current_ticket.ticket[i].amount);
		}
		//add this sum to the waiters total money and add to waiter's tables served
		tableServed(sum);
	   
		return waiter_input - sum;
	}

	//add the sum to the waiters total money
	function tableServed(sum) {
		for (var i=0; i<$rootScope.WAITERS.length;i++) {
			if ($rootScope.WAITERS[i].name == $rootScope.current_waiter) {
				$rootScope.WAITERS[i].profit = $rootScope.WAITERS[i].profit + sum;
				//add +1 to the tables served
				$rootScope.WAITERS[i].tables_served = $rootScope.WAITERS[i].tables_served + 1;
				break;
			}
		}
	}
                            
}])