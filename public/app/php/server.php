<?php
	require_once('models/waiters.php');
	require_once('models/products.php');

	$waiter = new Waiters();
	$product = new Products();

	if (isset($_POST['command'])) {
		if ($_POST['command'] == 'init_information') {		
			$waiters = $waiter->getAllWaiters();
			$json_results = json_encode($waiters);
			print_r($json_results);
		}
		else if ($_POST['command'] == 'add_worker') {
			$waiter->addWorker($_POST['name'],$_POST['password']
				,$_POST['position'],$_POST['profit']);
		}
		else if ($_POST['command'] == 'remove_worker') {
			$waiter->removeWorker($_POST['id']);
		}
		else if ($_POST['command'] == 'waiter_served') {
			$waiter->waiterServed($_POST['id'],$_POST['profit']);
		}	
		//products 	
		else if ($_POST['command'] == 'init_products') {		
			$products = $product->getAllProducts();
			$json_results = json_encode($products);
			print_r($json_results);
		}
		else if ($_POST['command'] == 'add_product') {
			$product->addProduct($_POST['name'],$_POST['category'],$_POST['sub_category'],$_POST['price']);
		}
		else if ($_POST['command'] == 'remove_product') {
			$product->removeProduct($_POST['id']);
		}
	}
/*	else if (isset($_POST['command'])) {
		if ($_POST['command'] == 'init_products') {		
			$products = $product->getAllProducts();
			$json_results = json_encode($products);
			print_r($json_results);
		}
		else if ($_POST['command'] == 'add_product') {
			$product->addProduct($_POST['name'],$_POST['category'],$_POST['sub_category'],$_POST['price']);
		}		
	} */
	/*
var waiters = [
        {name:'Kushtrim',password:'123',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Kreshnik',password:'456',logged_in:false,position:'Manager',profit:0,tables_served:0},
        {name:'Patriot',password:'789',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Nazif',password:'159',logged_in:false,position:'Waiter',profit:0,tables_served:0},
    ];
    */
?>
