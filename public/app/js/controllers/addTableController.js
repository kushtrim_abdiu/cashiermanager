angular.module('EliteApps')
.controller('addTableController', ['$scope','$rootScope','$mdDialog','$mdToast','Waiter','Setup','Business','Table',
function($scope,$rootScope,$mdDialog,$mdToast,Waiter,Setup,Business,Table) {
    $scope.choose_table_label = Setup.getString("choose_table_label");
    $scope.enter_customer_nr = Setup.getString("enter_customer_nr");
    //we put the tables variable into the available_tables variable because it is under scope
    //it means that it can bind with the HTML
    $scope.available_tables = Table.getAvailableTables();
    var current_waiter = $rootScope.currentWaiter;
    $scope.selectNumberOfClientsForm = false;
    $scope.maxNrOfCustomers = 20;

    //$scope.totalNrOfClients = 20;
    var newTable = {};
    newTable.printed = false;

    $scope.selectTable = function(table_nr) {
        $rootScope.category_is_selected = false;
        newTable.table = table_nr;
        newTable.waiter_id = current_waiter.id;
        newTable.name = current_waiter.name;
        $scope.selectNumberOfClientsForm = true;
    }

    $scope.numberOfPeople = function(nr_people) {
        newTable.people = nr_people;
        Table.openTable(newTable)
        .then(function(response) {
            Table.takeTable(response.data);
            //we store all the tables in TABLES global variable accessible by the html
            $rootScope.TABLES = Table.allTables;
            $rootScope.selected_table = newTable.table;
            //we have to notify the html that we have table so we show our tables
            //instead of the message that says no tables
            $rootScope.has_tables = true;
            $mdDialog.cancel();
            //we can display a Toast here saying the table is open
        });
    }

    //this function is triggered when the X button of the add table popup is clicked
    $scope.tableDialogExit = function(event) {
        $mdDialog.cancel();
    }

}]);
