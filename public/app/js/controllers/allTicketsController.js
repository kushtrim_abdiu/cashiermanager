angular.module('EliteApps')
.controller('allTicketsController', ['$scope','$rootScope','$location','$mdBottomSheet','$mdDialog','$mdToast','$rootElement','$mdSidenav','Table',
function($scope,$rootScope,$location,$mdBottomSheet,$mdDialog,$mdToast,$rootElement,$mdSidenav,Table) {
        $scope.all_tickets_from_today = $rootScope.LOCAL_TEXTS.all_tickets_from_today;

        $scope.ticketsDialogExit = function() {
            $mdDialog.cancel();
        }

        var date = new Date();
        date.setDate(date.getDate()-1); //yesturday
        Table.tryGetTicketsFromDate(date);
}]);
