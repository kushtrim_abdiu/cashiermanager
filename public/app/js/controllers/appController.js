
angular.module('EliteApps')
.controller('appController', ['$scope','$rootScope','$cookies','$location','$mdBottomSheet','$mdDialog',
'$mdToast','$rootElement','$mdSidenav','BusinessData','Business','Setup','Waiter','Item','Table',
function($scope,$rootScope,$cookies,$location,$mdBottomSheet,$mdDialog,$mdToast,
    $rootElement,$mdSidenav,BusinessData,Business,Setup,Waiter,Item,Table) {
    //GLOBAL VARIABLES
    $scope.COMPANY_NAME = "EliteApp";
    $scope.DATE = new Date();
    $rootScope.BUSINESS_IS_LOGGED_IN;
    $rootScope.LOCAL_TEXTS;
    $rootScope.TABLES;
    $rootScope.ITEMS;
    $rootScope.has_tables = false;
    $rootScope.currentWaiter; //its a hash with waiter information
    $rootScope.category_is_selected = false;
    $rootScope.category = '';
    $rootScope.SUB_CATEGORIES;
    //the items are in remove or select mode
    $scope.editable = true;

    //in app control variables
    $rootScope.side_menu_is_selected = false;
    $rootScope.selected_table = 0;

    $scope.settingsOptionsKey = ['settings_language','app_info','developer_info'];
    $scope.settingsOptions = [];
    //on app start - $viewContentLoaded is angularjs event which means when page has loaded
    $scope.$on('$viewContentLoaded', function() {
        //initiate the app
        initApp();

	});

    /** initApp - initializes the app strings and checks for the session
     *  @return void - if it has session it sets the app variables
     */
    function initApp() {
        //load the strings
        loadStrings();
        //then by checking the session
        //if the business has a session we dont show the login form
        //we move straight to the app
        Business.requestSession() //request session
        .then(Business.getSession) //get session
        .then(function(response) {  //handle sesion
            //console.log(response);
            if (response.data != undefined && response.data.status == 'success') {
                $rootScope.BUSINESS_IS_LOGGED_IN = Business.isLoggedIn;
                setAppVariables(response);
                $scope.login();
            }
        })
    }

    //it calls the methods from the Setup class
    //to get the business language and strings OR the default language of the app
    function loadStrings() {
        //we start the app by reading the app default strings
        Setup.requestStrings() //request the strings i.e. app texts
        .then(Setup.getStrings) // get the strings
        .then(function() {$rootScope.LOCAL_TEXTS = Setup.appStrings;}); //set the strings
    }


   /* logBusinessIn - gets the user input and makes e request to authenticate the business
    * the function is triggered when the user clicks the Business Login button
    * @param business - an object containing the username and the password from the input
    * @return void - it sets upp all the app objects if successful i.e. - waiters, items,tickets..
    */
    $scope.logBusinessIn = function(business) {
        //if the username and password werent specified
        if (business == 'undefined') {
            return;
        }
        //if business username and password are defined i.e. business object contains username and password
        Business.tryAuthenticate(business) //request to authenticate
        .then(Business.authenticationResponse)  //get the response
        .then(function(response) { //handle the response
            if (Business.isLoggedIn) {
                //if the business is succesfully authenticated
                //we setup the app variables
                setAppVariables(response);
                //then, the user gets redireted inside the app and the waiter login is shown
                $scope.login();
            }
            else {
                //the login was not succesful, we empty out the user input
                $scope.business = [];
            }
        });
    };

    //this function is triggered when sidenav icon is clicked
    //it toggles the side nav
    $scope.openWorkerSideNav = function(event) {
        $rootScope.side_menu_is_selected = true;
	    $mdSidenav('worker_sidenav').toggle();
	};

   /* login - logs the waiter in the app
    * shows the waiter login popup -
    * the methods that handle this view are defined in loginController file
    */
    $scope.login = function() {
        $mdDialog.show({
           controller: 'loginController',
	       templateUrl: 'views/log-in.html',
	    })
    }

    /* logout - logs the waiter out and shows the login form
     * so the other workers can loggin
     */
     $scope.logOut = function() {
         Waiter.logout();
         $rootScope.has_tables = Table.waiterHasTables($rootScope.currentWaiter.name);
         //reasign the current waiter object to empty
         $rootScope.currentWaiter = [];
         //reset other parameters
         $rootScope.category_is_selected = false;
         $rootScope.category = '';
         $rootScope.selected_table = 0;
         //show the login form
         $scope.login();
     }

   /* addTable - opens up the dialog where the waiter can select a table
    * shows the select an available table popup
    * the methods that handle this view are defined in addTableController file
    */
 	$scope.addTable = function(event) {
 		//show the popup with the currently free tables
 		$mdDialog.show({
 	      controller: 'addTableController',
 	      templateUrl: 'views/choose-table.html',
 	    })
    }

    //selectTable - selects the table on button click
    $scope.selectTable = function(table_nr) {
        $rootScope.selected_table = table_nr;
    }

   /* selectCategory - selects a category and then
    * shows the items on that category
    */
    $scope.selectCategory = function(category) {
        //first we have to make sure that there is a table selected
        if ($rootScope.selected_table != 0) {
			$rootScope.category = category;
			$rootScope.category_is_selected = true;
			//after a category is selected we have to fill the subcategory array with subcategories of that category
            $rootScope.SUB_CATEGORIES = Item.getSupCategoriesOfCategory(category);
		}
		else {
			$mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_table_selected_msg));
		}
    }

    /* addItemToTicket - its a function called when we click on an item
     * @param (int) - id : the id of the item
     * @param (string) - name : the name of the item
     * @param (double) - price : the price of the item
     */
     $scope.addItemToTicket = function(id,name,price) {
         Waiter.tryAddItemToTicket(id,name,price,$rootScope.selected_table)
         .then(function(ticket) {
             $rootScope.TABLES = Table.updateTableTicket(ticket.data);
         });
     }

     /* removeItem - called when the remove button is clicked
     * @param (int) - id : the id of the item we want to remove
     * @param (string) - name : the name of the item
     * @param (double) - price : the price of the item we are removing
     */
     $scope.removeItem = function(id,name,price) {
         var removeItemInfo = {worker_id:$rootScope.currentWaiter.id,table:$rootScope.selected_table,
            item_id:id,item_name:name,item_price:price};
         Table.tryRemoveItem(removeItemInfo)
         .then(Table.removeItem);
     }

     $scope.togglecheckItem = function(item_id) {
         Table.tryToggleCheckItem(item_id)
         .then(Table.handleToggleCheckedItem)
         .catch(Table.handleFailToggleItem); //if for some reason the connection is lost we should reset the check
     }

    /* payTicket - called when we want to pay and close the ticket
     *
     */
     $scope.payTicket = function() {
         Table.getTableSum($rootScope.selected_table)
         .then(function(tableSum) {
             if (tableSum.data > 0) {
                 if (Table.getOnlyTheSumOfSelectedItems() > 0) {
                     $rootScope.tmpTotalSum = tableSum.data;
                     //we should show the payment dialog
                     $mdDialog.show({
                        controller: 'paymentController',
             	       templateUrl: 'views/add-payment.html',
             	    })
                }
                else {
                    $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_item_to_print));
                }
             }
             else {
                 $mdToast.show($mdToast.simple().content('Ticket is empty!'));
             }
         });
     }

    /* changePin - called from the view when we want to change our pin
     *
     */
     $scope.changePin = function() {
         //show the popup with the currently free tables
  		$mdDialog.show({
  	      controller: 'changePinController',
  	      templateUrl: 'views/change-pin.html',
  	    })
     }

     /*removeTicket - deletes the ticket
     * if the ticket is not empty then it shows a form where we have to register why the ticket
     * is being closed otherwise we just close it
     */
     $scope.removeTicket = function() {
         //first make sure we have selected a table
         if ($rootScope.selected_table != 0) {
            //check the number of items of the ticket
            //if there is at least one ticket then we have to show a pop up
            //where we can select the reason we are removing the item from the list
            //console.log($rootScope.TABLES[$rootScope.selected_table-1]);
            if ($rootScope.TABLES[$rootScope.selected_table-1].ticket.length > 0) {
                //there are items we should show the pop up to select
                //the reason why we are removing it
                $mdDialog.show({
          	      controller: 'removeTicketController',
          	      templateUrl: 'views/remove-ticket.html',
          	    })
            }
            else {
                //there is no items we can remove the item with no problem
                Table.tryRemoveTicket($rootScope.selected_table,$rootScope.currentWaiter.id,'')
                .then(Table.releaseTable)
                .then(Table.handleRemoveTicket);
            }
            //Table.removeTicket($rootScope.selected_table);
         }
         else {
             $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_table_selected_msg));
         }
     }

     $scope.settings = function(option) {
         $rootScope.selectedOption = option;
         //change languge
         $mdDialog.show({
           controller: 'settingsController',
           templateUrl: 'views/settings-options.html',
         })
     }

     $scope.viewAllTickets = function() {
         //show the form to view all the tickets
         $mdDialog.show({
           controller: 'allTicketsController',
           templateUrl: 'views/all-tickets.html',
         })
     }

  /* HELPER FUNCTIONS */
  /* setAppVariables - is called after a succesful login
   * @param - object (response) : is the response with all the app data from the server
   * @return - void : it sets the Waiter, Item and Ticket Objects
   */
   function setAppVariables(response) {
       handleLanguage(response.data.business.language);
       //console.log(response.data); //we activate the logg to help us debug
       Waiter.setBusinessWaiters(response.data.workers);
       Item.setBusinessItems(response.data.items);
       $rootScope.ITEMS = Item.allItems;
       Table.setUpTables(response.data.business.nr_tables,response.data.tickets);
       //set tickets to the tables they belong to
       Table.setUpTickets(response.data.tickets)
       $rootScope.TABLES = Table.allTables;
       //Table.setOpenTables(response.data.tickets);
       //$rootScope.has_tables = Table.hasTables;
       //$rootScope.TABLES = Table.allTables;
       //after all the data has been set we redirect to the app
       $rootScope.BUSINESS_IS_LOGGED_IN = true;
   }

   //sets the cookie
   function handleLanguage(language) {
       if (language != Setup.appStringsLanguage) {
           //we have to get the business specified language
           Setup.requestStringsForLanguage(language) //request the strings i.e. app texts
           .then(Setup.getStrings) // get the strings
           .then(function() {$rootScope.LOCAL_TEXTS = Setup.appStrings;}); //set the strings
       }
   }

}])

//the directive of sub categories where we have the tabs
.directive('subCategories', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/sub-categories.html',
    	controller: 'appController'
 	};
})
// the directive of side-menu with waiter info
.directive('sideMenu', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/side-menu.html',
    	controller: 'appController'
 	};
})
//API RESULTS
//THEY ARE REPLACED NOW BY REAL DATA
//the app texts for translation purposes
var app_texts = {
        'no_tables_msg' : 'NUK KENI TAVOLINE',
        'food_label' : 'USHQIM',
        'drinks_label' : 'PIJE',
        'snacks_label' : 'SNACKS',
        'other_label' : 'TJERA',
        'no_category_selected_msg' : 'NUK KENI ZGJEDHUR KATEGORI',
        'choose_table_label' : 'ZGJIDH TAVOLINE',
        'login_label' : 'LOGOHU',
        'no_table_selected_msg' : 'NUK KENI ZGJEDHUR ASNJE TAVOLINE',
        'pay' : 'PAGO',
        'print' : 'PRINTO',
        'ticket' : 'TIKETA',
        'selected_table_label' : 'TAVOLINA:',
        'waiter_info_label' : 'INFORMATA PER KAMARIERIN',
        'tables_served_label' : 'tavolina te servuara',
        'waiter_action_label' : 'OPCIONE',
        'change_ping_label' : 'NDRYSHO PININ',
        'create_reservation_label' : 'KRIJO REZERVIM',
        'add_new_product_label' : 'SHTO PRODUKT TE RI',
        'punch_out_label' : 'MBYLL',
        'register_payment_label' : 'PAGO',
        'enter_client_payment_label' : 'HYRJE',
        'return_amount_label' : 'KTHE KLIENTIT',
        'empty_ticket' : 'TIKETA ESHTE BOSH',
        'no_sufficient_money' : 'NUK KENI PARA TE MJAFTUESHME',
        'view_all_tickets' : 'SHIKO TIKETAT'
   };
/*
var items = [
		{name:'Coke', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Pepsi', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Fanta', category:'drink', sub_category:'non-alcoholic', price:2},
		{name:'Sprite', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Mineral Water', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Apple Juice', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Orange Juice', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Lemonade', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Machiato', category:'drink', sub_category:'coffee', price:2},
		{name:'Latte', category:'drink', sub_category:'coffee', price:2},
		{name:'Black Coffee', category:'drink', sub_category:'coffee', price:2},
		{name:'Capuchinno', category:'drink', sub_category:'coffee', price:2},
		{name:'Nescaffe', category:'drink', sub_category:'coffee', price:2},
		{name:'Apple Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Fruit Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Grapes Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Chicken Burger', category:'food', sub_category:'fast-food', price:22},
		{name:'Hamburger', category:'food', sub_category:'fast-food', price:22},
		{name:'Chicken Cheese', category:'food', sub_category:'fast-food', price:22},
		{name:'French Fries', category:'food', sub_category:'fast-food', price:22}
	];
*/
var tables = [
        {nr:'1',available:true,waiter:"free", ticket:[],total:0},
        {nr:'2',available:true,waiter:"free", ticket:[],total:0},
        {nr:'3',available:true,waiter:"free", ticket:[],total:0},
        {nr:'4',available:true,waiter:"free", ticket:[],total:0},
        {nr:'5',available:true,waiter:"free", ticket:[],total:0},
        {nr:'6',available:true,waiter:"free", ticket:[],total:0},
        {nr:'7',available:true,waiter:"free", ticket:[],total:0},
        {nr:'8',available:true,waiter:"free", ticket:[],total:0},
        {nr:'9',available:true,waiter:"free", ticket:[],total:0},
        {nr:'10',available:true,waiter:"free", ticket:[],total:0},
        {nr:'11',available:true,waiter:"free", ticket:[],total:0},
        {nr:'12',available:true,waiter:"free", ticket:[],total:0}
     ];
/*
var waiters = [
        {name:'Kushtrim',password:'123',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Kreshnik',password:'456',logged_in:false,position:'Manager',profit:0,tables_served:0},
        {name:'Patriot',password:'789',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Nazif',password:'159',logged_in:false,position:'Waiter',profit:0,tables_served:0},
    ];
*/
