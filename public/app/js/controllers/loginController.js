angular.module('EliteApps')
.controller('loginController', ['$scope','$rootScope','$mdDialog','$mdToast','Waiter','Setup','Table',
function($scope,$rootScope,$mdDialog,$mdToast,Waiter,Setup,Table) {
        $scope.login_label = $rootScope.LOCAL_TEXTS.login_label;
        $scope.user_input = "";

        //this button is triggered when the waiter presses a number in the login keypad
        $scope.inputClicked = function(nr) {
            $scope.user_input = Waiter.updateWaiterInput(nr);
        };

        //this button is triggered after the user has written the password and pressed login
        $scope.logMeIn = function() {
           //check if we have any pin matching the user input
           Waiter.logWaiterIn($scope.user_input);
           //check if the login was succesful - > i.e. if we loggedWaiter is set
           if (Waiter.isLoggedIn()) {
               $rootScope.currentWaiter = Waiter.loggedWaiter;
               //console.log($rootScope.currentWaiter);
               //$rootScope.has_tables = Table.waiterHasTables(Waiter.loggedWaiter.name);
               $rootScope.has_tables= Table.waiterHasTables(Waiter.loggedWaiter.name);
               $mdDialog.cancel();
           }
           else {
               //we can output a string here saying that the pin was wrong and empty out user input
               $scope.user_input = "";
               Waiter.loginInput = "";
               $mdToast.show($mdToast.simple().content(Setup.getString("wrong_password_msg")));
           }
        };

}])
