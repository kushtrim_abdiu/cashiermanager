angular.module('EliteApps')
.controller('removeTicketController', ['$scope','$rootScope','$mdDialog','$mdToast','Table',
function($scope,$rootScope,$mdDialog,$mdToast,Table) {
    $scope.strings = $rootScope.LOCAL_TEXTS;
    $scope.closePaymentDialog = function() {
        $mdDialog.cancel();
    }

    $scope.removeTicket = function() {
        if ($scope.ticket.remove_reason != undefined) {
            if ($scope.ticket.remove_reason !='other') {
                var message = $scope.ticket.remove_reason;
            }
            else {
                var message = $scope.ticket.remove_message;
            }
            Table.tryRemoveTicket($rootScope.selected_table,$rootScope.currentWaiter.id,message)
            .then(Table.handleRemoveTicket);
            $mdDialog.cancel();
        }
        else {

        }
    }

}]);
