angular.module('EliteApps')
.controller('settingsController', ['$scope','$rootScope','$mdDialog','$mdToast','Setup',
function($scope,$rootScope,$mdDialog,$mdToast,Setup) {

    $scope.option = $rootScope.selectedOption;
    $scope.strings = $rootScope.LOCAL_TEXTS;
    $scope.selectedLanguage = $scope.strings.language;

    if ($scope.option == 'language') {
        $scope.settings_title_label = $scope.strings.settings_language;
    }
    else if ($scope.option == 'app_info') {
        $scope.settings_title_label = $scope.strings.app_info;
    }
    else if ($scope.option == 'developer_info') {
        $scope.settings_title_label = $scope.strings.developer_info;
    }

    $scope.optionDialogExit = function() {
        $mdDialog.cancel();
    }

    $scope.changeLanguage = function(selectedLang) {
        Setup.requestStringsForLanguage(selectedLang) //request the strings i.e. app texts
        .then(Setup.getStrings) // get the strings
        .then(function() {$rootScope.LOCAL_TEXTS = Setup.appStrings;}); //set the strings

        $mdDialog.cancel();
    }

}]);
