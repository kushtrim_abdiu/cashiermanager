angular.module('EliteApps')
.controller('changePinController', ['$scope','$rootScope','$mdDialog','$mdToast','Waiter','Setup',
function($scope,$rootScope,$mdDialog,$mdToast,Waiter,Setup) {
    $scope.login_label = $rootScope.LOCAL_TEXTS.login_label;
    $scope.error_label = $rootScope.LOCAL_TEXTS.new_pin_error;
    $scope.user_input = "";
    $scope.pinError = false;
    //the input value might be prefilled from our loggin input thats why we reset it to empty
    Waiter.resetInput();
    //this button is triggered when the waiter presses a number in the login keypad
    $scope.inputClicked = function(nr) {
        $scope.user_input = Waiter.updateWaiterInput(nr);
    };

    $scope.tryChangingPin = function() {
        //we have to make sure the user input is 4 digits
        if (Waiter.inputIsValid($scope.user_input)) {
            //if the input is valid we can send it to the server and
            //try to update the pin
            Waiter.tryChangingPin()
            .then(function(response) {
                if (response.data.status == 'exist') {
                    console.log('exists');
                    $scope.pinError = true;
                }
                else if(response.data.status == 'success') {
                    //we have to close the dialog and show a toast saying your new pin is...
                    console.log('success');
                    $mdDialog.cancel();
                    $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.pin_updated));
                }
                else {
                    $mdDialog.cancel();
                    //we should say something went wrong try again later
                    console.log('fail');
                }
            });

        }
        else {
            $scope.pinError = true;
        }
    }

    $scope.tableDialogExit = function() {
        $mdDialog.cancel();
    }

}]);
