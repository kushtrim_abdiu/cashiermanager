angular.module('EliteApps')
.controller('adminController',['$scope','$rootScope','$location','$mdBottomSheet','$mdDialog','$mdToast','$rootElement','$mdSidenav','BusinessData',
function($scope,$rootScope,$location,$mdBottomSheet,$mdDialog,$mdToast,$rootElement,$mdSidenav,BusinessData) {

    //global variables
    $scope.DATE = new Date();
    $rootScope.COMPANY_NAME = 'EliteCaffe';
    $rootScope.IS_LOGGED_IN = false;
    $rootScope.LOCAL_TEXTS;
    $rootScope.PRODUCTS;
    $rootScope.TABLES;
    $rootScope.SUB_CATEGORIES = [];
    $scope.WORKERS;

    $scope.has_admin_session = false;
    $scope.admin_username = "";
    $scope.admin_password = "";
    $scope.business_logged_in = "Kafe Bar ";
    $scope.worker_name = "";
    $scope.product_name = "";

    //on page load
    $scope.$on('$viewContentLoaded', function() {
        //check if we have anyone logged in
        //$scope.LOCAL_TEXTS = texts;
        $scope.retrieveData();
        $scope.TakeData();
	});

    $scope.initiateBusiness = function() {
        BusinessData.authenticate({"username":'elitebar',"password":'123456'}).done(function(data){
            console.log(data);
        });
        /*
        BusinessData.getBusinessData().done(function(data){
            $rootScope.WAITERS = data[0].waiters;
            $rootScope.TABLES = data[0].tables;
            $rootScope.LOCAL_TEXTS = data[0].strings[0];
            $rootScope.ITEMS = data[0].items;
        }); */
    };

    //log admin in
    $scope.logAdminIn = function() {
        if ($scope.admin_username == 'admin' && $scope.admin_password == 'admin') {
            $scope.has_admin_session = true;
        }
    };

     //get data from the server
    $scope.retrieveData = function() {
        $scope.WORKERS = [];
        var jqxhr = $.post( "php/server.php",
            {command:'init_information'},function() {})
        .done(function(data) {
            var results = jQuery.parseJSON(data);
            $scope.WORKERS = results;
            $scope.$apply();

        })
    };

     $scope.TakeData = function() {

        var jqxhr = $.post( "php/server.php",{command:'init_products'},function() {})
        .done(function(data) {
            var results = jQuery.parseJSON(data);
            $scope.PRODUCTS = results;
            $scope.$apply();

        })
    };


    $scope.addWorker = function() {
        var jqxhr = $.post( "php/server.php",
        {command:'add_worker',name:$scope.worker_name,password:$scope.worker_password,
        position:$scope.worker_position,profit:$scope.worker_profit},function() {})
        .done(function(data) {
            //var results = jQuery.parseJSON(data);
           // $scope.WORKERS = results;
            //$scope.$apply();
            $scope.WORKERS = [];
            $scope.retrieveData();
            $scope.worker_name = '';
            $scope.worker_position = '';
            $scope.worker_password = '';
            $scope.worker_profit = '';
        })

    };

     $scope.addProduct = function() {
        var jqxhr = $.post( "php/server.php",{command:'add_product',name:$scope.product_name,category:$scope.product_category,sub_category:$scope.product_sub_category
            ,price:$scope.product_price},function() {})
        .done(function(data) {
            //var results = jQuery.parseJSON(data);
            //$scope.PRODUCTS = results;

            //$scope.$apply();
            $scope.PRODUCTS = [];
            $scope.TakeData();
            $scope.product_name = '';
            $scope.product_category = '';
            $scope.product_sub_category = '';
            $scope.product_price = '';

        })
    };

    $scope.removeWorker = function(id) {
        var jqxhr = $.post( "php/server.php",
        {command:'remove_worker',id:id},function() {})
        .done(function(data) {
           $scope.WORKERS = [];
           $scope.retrieveData();
        })
    };
    $scope.removeProduct = function(id) {
        var jqxhr = $.post( "php/server.php",
        {command:'remove_product',id:id},function() {})
        .done(function(data) {
           $scope.PRODUCTS = [];
           $scope.TakeData();
        })
    };

    //logout
    $scope.logAdminOut = function() {
        $location.path('/app');
    };

}])
.directive('adminItems', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/admin-items.html',
    	controller: 'adminController'
 	};
})
.directive('adminStatistics', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/admin-statistics.html',
    	controller: 'adminController'
 	};
})
.directive('adminWorkers', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/admin-workers.html',
    	controller: 'adminController'
 	};
})

var texts = {
    admin_not_logged_in : "ADMINISTRATORI NUK ESHTE LOGUAR",
    admin_login_text : "ADMINISTRATOR",
    login_button_label : "LOGOHU",
    logout_label : "Logout",
    statistics_label : "STATISTIKA",
    workers_label : "PUNTORET",
    items_label : "PRODUKTET"
};
