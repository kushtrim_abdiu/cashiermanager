angular.module('EliteApps')
.controller('paymentController', ['$scope','$rootScope','$location','$mdBottomSheet','$mdDialog','$mdToast','$rootElement','$mdSidenav','BusinessData','Table','Waiter',
function($scope,$rootScope,$location,$mdBottomSheet,$mdDialog,$mdToast,$rootElement,$mdSidenav,BusinessData,Table,Waiter) {
     //button add table is clicked
     $scope.showKeyboard = true;
     $scope.strings = $rootScope.LOCAL_TEXTS;
     $scope.waiter_input = "";
     $scope.sumOfCheckedItems = Table.getOnlyTheSumOfSelectedItems();

     //triggered when the button to show or hide keyboard is clicked
     $scope.toggleKeyboard = function() {
         $scope.showKeyboard = !$scope.showKeyboard;
     }

     //when users click on the any key pad to enter the sum
     //that the client is paying
     $scope.inputClicked = function(nr){
         //if backspace is not clicked
         if (nr!=-1) {
             $scope.waiter_input +=nr;
         }
         else {
            $scope.waiter_input = removeLastDigit($scope.waiter_input);
         }
     }

     //when enter is pressed it triggers the calculateReturn function
     $scope.enterPressed = function(ev) {
         if (ev.which === 13) {
             //enter pressed
             $scope.calculateReturn();
         }
     }

     //is triggered when we press enter or we press the send button in the keypad
     $scope.calculateReturn = function() {
         //calculate returning amount -> input - the sum of the items checked (to be payed)
         $scope.return_amount = $scope.waiter_input - $scope.sumOfCheckedItems/*$rootScope.tmpTotalSum*/;
         if ($scope.return_amount >= 0) {
             //we have to store the ticket in the database and free the table
             Table.closeTable($rootScope.selected_table)
             .then(Table.handleCloseTable);
             /*.then(function(tickets) {
                // Table.setUpTickets(tickets.data);
                console.log('this is that shit');
                console.log(tickets);
            });*/
             //show the return amount
             $scope.returnAmountBool = true;
         }
         else {
             $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_sufficient_money));
             $scope.waiter_input ='';
         }
     }

     //triggered when X is clicked when payment is finalized
     $scope.closePaymentDialog = function() {
         $mdDialog.cancel();
     }

     $scope.tableDialogExit = function(event) {
         var isClosed = false;
         if (!Table.hasUnprintedTickets) {
             $rootScope.current_ticket = [];
             //we have to release the table and reset its ticket
             releaseTable();
             isClosed = true;
         }
         else {
             //the ticket is not closed yet
         }

         $rootScope.WAITERS = Waiter.tableServed(isClosed, $scope.sumOfCheckedItems);

         $mdDialog.cancel();
     }

     //maybe this function should be in tables class
     var releaseTable = function() {
         $rootScope.TABLES.forEach(function(table,index,array) {
             if (table.nr == $rootScope.selected_table) {
                 table.waiter = 'free';
                 table.available = true;
                 table.ticket = [];
                 table.total = 0;
                 $rootScope.selected_table = 0;
                 $rootScope.category_is_selected = false;
             }
         });

         $rootScope.has_tables = Table.waiterHasTables($rootScope.currentWaiter.name);
     }

     //function which removes the last digit from the waiter input
     //this function is triggered when X is clicked
 	function removeLastDigit(input) {
 	 	var new_input = "";
 	 	for (var i=0; i<input.length-1; i++) {
 	 		new_input += input[i];
 	 	}

 	 	return new_input;
 	}

}])
