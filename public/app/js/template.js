
angular.module('EliteApps')
.controller('appController', ['$scope','$rootScope','$location','$mdBottomSheet','$mdDialog','$mdToast','$rootElement','$mdSidenav','BusinessData',
function($scope,$rootScope,$location,$mdBottomSheet,$mdDialog,$mdToast,$rootElement,$mdSidenav,BusinessData) {
    //global variables
    $scope.DATE = new Date();
    $rootScope.COMPANY_NAME = 'Kafe Bar';
    $rootScope.IS_LOGGED_IN = false;
    $rootScope.BUSINESS_IS_LOGGED_IN;// = false;
    $rootScope.LOCAL_TEXTS;
    $rootScope.ITEMS;
    $rootScope.TABLES;// = tables;
    //new implementation of tables
    $rootScope.ALL_TABLES;
    $rootScope.SUB_CATEGORIES;
    //$rootScope.WAITERS = waiters;
    $rootScope.WAITERS;
   // $rootScope.CLOSED_TICKETS = [];
    ////app variables ////////
    $rootScope.selected_table = 0;
    $rootScope.current_waiter = '';
    $rootScope.current_ticket;
    $rootScope.has_tables = true;
    $rootScope.category_is_selected = false;
    $rootScope.side_menu_is_selected = false;
    $scope.editable = true;
   // $scope.printOptionOpen = true;
    /////// ////////////// /////////
    //on page load
    $scope.$on('$viewContentLoaded', function() {
        //get the session;
        $scope.checkSession();
        //this should happen after the item details and company details have been fully loaded
        $scope.initApp();
    	//show waiter login
        if ($rootScope.current_waiter == '' && $rootScope.BUSINESS_IS_LOGGED_IN) {
            //no one is identified so show the login form
            $scope.login();
        }
	});

    //initiate the application, get the strings for the app
    $scope.initApp = function() {
        BusinessData.getAppStrings().done(function(strings){
            $rootScope.LOCAL_TEXTS = strings[0];
        });
    };

    $scope.checkSession = function() {
        BusinessData.getSession().done(function(res){
            if (res.status == 'success') {
                $rootScope.WAITERS = initiateWaiters(res.workers);
                //$rootScope.TABLES = res.tables;
                $rootScope.TABLES = prepareTables(res.business.nr_tables);
                $rootScope.ITEMS = res.items;
                $rootScope.BUSINESS_IS_LOGGED_IN = true;
                retrieveTablesFromSession(res.tickets);
                $scope.login();
            }
        });
    };

    $scope.logBusinessIn = function(business) {
        if (typeof business === 'undefined') {
            return;
        }
        BusinessData.authenticate({"username":business.username,"password":business.password}).done(function(data){
            if (data.status == 'success') {
                $rootScope.COMPANY_NAME = data.business.username;
                $rootScope.WAITERS = initiateWaiters(data.workers);
                $rootScope.BUSINESS_IS_LOGGED_IN = true;
                $rootScope.TABLES = prepareTables(data.business.nr_tables);
                $rootScope.ITEMS = data.items;
                $scope.login();
            }
            else {
                $scope.loginFail = true;
                //we have to redirect to login again
                $rootScope.BUSINESS_IS_LOGGED_IN = false;
                $scope.business.username = "";
                $scope.business.password = "";
            }

            $rootScope.$apply();
        });
    };

    $scope.selectCategory = function(category) {
        if ($rootScope.selected_table != 0) {
            $rootScope.category_is_selected = true;
            $rootScope.SUB_CATEGORIES = [];
            for (var key in $rootScope.ITEMS) {
                if ($rootScope.ITEMS[key].category == category) {
                    if ($rootScope.SUB_CATEGORIES.indexOf($rootScope.ITEMS[key].sub_category) == -1) {
                        $rootScope.SUB_CATEGORIES.push($rootScope.ITEMS[key].sub_category);
                    }
                }
            }
            $scope.sub_categories = $rootScope.SUB_CATEGORIES;
        }
        else {
            //show the message no table is selected
            $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_table_selected_msg));
        }
    }

    //waiter identification
    $scope.login = function($scope) {
		$mdDialog.show({
	      controller: loginController,
	      templateUrl: 'views/log-in.html',
	    })

	    function loginController($scope) {
            $scope.login_label = $rootScope.LOCAL_TEXTS.login_label;
	    	$scope.user_input="";
            //login button is clicked
	    	$scope.logMeIn = function() {
	           //check if we have any pin matching the user input
                $scope.waiters = $rootScope.WAITERS;
                var new_waiter = sucessfullAuth($scope.user_input);
                if (new_waiter != '') {
                    $rootScope.current_waiter = new_waiter;
	    			$mdDialog.cancel();
                }
                else {
                    $scope.user_input = '';
                }
	    	};
	    	//when users click on the button
	    	$scope.inputClicked = function(nr) {
	    		//if backspace is not clicked
	    		if (nr!=-1) {
	    			$scope.user_input += nr;
	    		}
	    		else {
                    var new_input="";
                    for (var i=0; i<$scope.user_input.length-1; i++) {
                        new_input += $scope.user_input[i];
                    }
	    			$scope.user_input = new_input;
	    		}
	    	}
            $scope.goToAdmin = function() {
                $location.path("/admin");
                $mdDialog.cancel();
            };
	    }
	};

    //logout
    $scope.logOut = function() {
        for (var key in $rootScope.WAITERS) {
            if ($rootScope.WAITERS[key].logged_in) {
                $rootScope.WAITERS[key].logged_in = false;
                $rootScope.selected_table = 0;
                $rootScope.sub_categories = [];
                $rootScope.category_is_selected = false;
                $rootScope.current_waiter = '';
                $scope.login();
                //close the sidenav if it is open
                $rootScope.side_menu_is_selected = false;
                $mdSidenav('worker_sidenav').toggle();
                break;
            }
        }
    }

    //button add table is clicked
	$scope.addTable = function(event) {
		//show the popup with tables 1
		$mdDialog.show({
	      controller: addTableController,
	      templateUrl: 'views/choose-table.html',
	    })
	    function addTableController($scope) {
            $scope.choose_table_label = $rootScope.LOCAL_TEXTS.choose_table_label;
            $scope.all_tables = $rootScope.TABLES;
			$scope.addButton = function(button_nr) {
                for (var i = 1; i <= $rootScope.TABLES.length; i++) {
                    if ($rootScope.TABLES[i].nr == button_nr) {
                        $rootScope.TABLES[i].waiter = $rootScope.current_waiter;
                        $rootScope.has_tables = true;
                        $mdDialog.cancel();
                        $rootScope.selected_table = button_nr;
                        var table_info = {"table":button_nr,"waiter":$rootScope.current_waiter,
                            "waiter_id":getWaiterID()};
                        BusinessData.openTicket(table_info).done(function(res){});
                        break;
                    }
                }
			}
			$scope.tableDialogExit = function(event) {
                $mdDialog.cancel();
			}
		}
	}

    //select a table that is added
    $scope.selectTable = function (table_nr) {
        $rootScope.current_ticket = $rootScope.TABLES[table_nr];
        $rootScope.selected_table = table_nr;
        $rootScope.category_is_selected = false;
        $scope.SUB_CATEGORIES = [];
        //we have to create a session with the current user and the table and orders
    }

    $scope.addItemToTicket = function(item_id,item_name,item_price) {
        var new_item = {id:item_id,name:item_name,amount:1,sell_price:item_price,checked:true,
            "table":$rootScope.selected_table};
        console.log('current waiter:'.$rootScope.current_waiter);
        console.log('preparing the new item');
        console.log(new_item);
    /*    BusinessData.addItemToTicket(new_item).done(function(data){
            console.log('this is the response...');
            console.log(data);
            console.log('this is the response...');
            for (var key in $rootScope.TABLES) {
                if ($rootScope.TABLES[key].nr == $rootScope.selected_table) {
                    //console.log($rootScope.TABLES[key]);
                }
            }
        });
        //first we have to check if the ticket is empty
		/*var new_item = {id:item_id,name:item_name,amount:1,sell_price:item_price,checked:true,"table":$rootScope.selected_table};
		var new_ticket = [];
		var flag = false ;
        for (var key in $rootScope.TABLES) {
            if ($rootScope.TABLES[key].nr == $rootScope.selected_table) {
                //mark the selected ticket
				$rootScope.current_ticket = $rootScope.TABLES[key];
                console.log($rootScope.TABLES[key]);
                //we have to prepare the array that we will insert into the tickets of the currently selected table
				//if the length is zero it means there is no item in the ticket
				if ($rootScope.TABLES[key].ticket.length!=0) {
                    for (var j=0;j<$rootScope.TABLES[key].ticket.length;j++) {
			 			//check if the item already exists in the ticket
			 			if (item_exists_in_ticket($rootScope.TABLES[key].ticket[j].name,item_name)) {
			 				flag = true;
			 				break;
			 			}
				 	}
                    if (flag) {
						var amount = $rootScope.TABLES[key].ticket[j].amount;
                        BusinessData.changeItemTicketAmount(
                            {"amount":amount+1,"ticket":$rootScope.current_ticket}).done(function(data){});
		 				$rootScope.TABLES[key].ticket[j].amount = amount + 1;
		 				flag = false;
					}
					else {
						$rootScope.TABLES[key].ticket.push(new_item);
					}
                }
                else {
					//the ticket is empty so we dont need to check if the item exists or not we should just add it
					new_ticket.push(new_item);
					$rootScope.TABLES[key].ticket = new_ticket;
				}
                //add it to the server
                BusinessData.addItemToTicket(new_item).done(function(data){});
				//update the price of the ticket
				$rootScope.TABLES[key].total += parseFloat(item_price);
            }
        } */
    };

    $scope.removeItem = function(name,price) {
        for (var i=1 ;i < $scope.TABLES.length; i++) {
    		if ($rootScope.TABLES[i].nr == $rootScope.selected_table) {
    			for(var j=0; j<$rootScope.TABLES[i].ticket.length;j++) {
    				if ($rootScope.TABLES[i].ticket[j].name == name) {
    					//now if the amount is one then remove all the item from the list
    					if ($rootScope.TABLES[i].ticket[j].amount ==1) {
    						$rootScope.TABLES[i].ticket.splice(j, 1);
    					}
    					else {
    						$rootScope.TABLES[i].ticket[j].amount --;
    					}
    					$rootScope.TABLES[i].total -= price;
    					break;
    				}
    			}
    		}
    	}
    };

    $scope.openWorkerSideNav = function(event) {
        $rootScope.side_menu_is_selected = true;
	    $mdSidenav('worker_sidenav').toggle();
	};

    //print the selected ticked with the items that are checked
    $scope.printTicket = function() {
        //$scope.printOptionOpen = true;
    };

    //pay the ticked and free the table
    $scope.payTicket = function() {
        //show the popup with tables
        if ($rootScope.selected_table != 0 && getTicketItemCount($rootScope.selected_table) != 0) {
            $mdDialog.show({
              controller: 'paymentController',
              templateUrl: 'views/add-payment.html'
            })
        }
        else {
            if (getTicketItemCount($rootScope.selected_table) == 0) {
                $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.empty_ticket));
            }
            else {
                $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_table_selected_msg));
            }
        }
    }

    //view all the tickets
    $scope.viewAllTickets = function() {
        $mdDialog.show({
          controller: 'allTicketsController',
          templateUrl: 'views/ticket-view.html'
        })
    };

    ///////// HELPER FUNCTIONS /////////////////////////////
    function sucessfullAuth(user_input) {
        for (var key in $rootScope.WAITERS) {
            if ($rootScope.WAITERS[key].password == user_input) {
                $rootScope.WAITERS[key].logged_in = true;
                return $rootScope.WAITERS[key].name;
            }
        }

        return '';
    }

    function getTicketItemCount(selected_table) {
        for (var i=1; i <= $scope.TABLES.length; i++) {
    		if ($rootScope.TABLES[i].nr == selected_table) {
                return $rootScope.TABLES[i].ticket.length
            }
        }
    }

    function item_exists_in_ticket(item_name_in_ticket,item_name) {
		var item_in_ticket = item_name_in_ticket.replace(" ","");
		var item = item_name.replace(" ","");
		if (item_in_ticket.toLowerCase() == item.toLowerCase()) {
			return true;
		}
		else {
			false;
		}
	}

    //at the begining we might not have the profit of the worker for that day so we initiate it to zero
    function initiateWaiters(waiters) {
        for (var key in waiters) {
            if (!waiters[key].profit) {
                waiters[key].profit = 0;
            }
        }
    /*
        for (var i = 0; i < waiters.length; i++) {
            console.log('...........');
            console.log(workers['name']);
            console.log('...........');
            if (!waiters[i].profit) {
                waiters[i].profit = 0;
            }
        } */
        return waiters;
    }

    function retrieveTablesFromSession(session_tables) {
        session_tables.forEach(function(e,index,array) {
            var index = e['table'];
            $rootScope.TABLES[index]["waiter"] = e['waiter'];
            $rootScope.TABLES[index]["available"] = false;
            $rootScope.TABLES[index]['ticket'] = e['items'];
            $rootScope.TABLES[index]['checked'] = true;
            $rootScope.TABLES[index]['total'] = e['total_price'];
        });
    }

    function prepareTables(nr_tables) {
        var all_tables = [];
        //we have to make an array with these much tables
        for (var i = 1; i <= nr_tables; i++) {
            all_tables[i] = {
                "nr" : i,
                "available" : true,
                "waiter" : "free",
                "ticket" : [],
                "total" : 0
            }
        }
        $rootScope.ALL_TABLES = all_tables;
        //for the moment we return the ALL_TABLES variable just for the work around
        //so we wont break the rest of the code
        return $rootScope.ALL_TABLES;
    }

    function getWaiterID()
    {
        $rootScope.WAITERS.forEach(returnWaiter);

        function returnWaiter(waiter,index,array) {
            console.log(waiter);
        }
    }
}])
//the directive of sub categories where we have the tabs
.directive('subCategories', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/sub-categories.html',
    	controller: 'appController'
 	};
})
// the directive of side-menu with waiter info
.directive('sideMenu', function(){
    return {
    	restrict:'E',
    	templateUrl:'views/side-menu.html',
    	controller: 'appController'
 	};
})
//API RESULTS

//the app texts for translation purposes
var app_texts = {
        'no_tables_msg' : 'NUK KENI TAVOLINE',
        'food_label' : 'USHQIM',
        'drinks_label' : 'PIJE',
        'snacks_label' : 'SNACKS',
        'other_label' : 'TJERA',
        'no_category_selected_msg' : 'NUK KENI ZGJEDHUR KATEGORI',
        'choose_table_label' : 'ZGJIDH TAVOLINE',
        'login_label' : 'LOGOHU',
        'no_table_selected_msg' : 'NUK KENI ZGJEDHUR ASNJE TAVOLINE',
        'pay' : 'PAGO',
        'print' : 'PRINTO',
        'ticket' : 'TIKETA',
        'selected_table_label' : 'TAVOLINA:',
        'waiter_info_label' : 'INFORMATA PER KAMARIERIN',
        'tables_served_label' : 'tavolina te servuara',
        'waiter_action_label' : 'OPCIONE',
        'change_ping_label' : 'NDRYSHO PININ',
        'create_reservation_label' : 'KRIJO REZERVIM',
        'add_new_product_label' : 'SHTO PRODUKT TE RI',
        'punch_out_label' : 'MBYLL',
        'register_payment_label' : 'PAGO',
        'enter_client_payment_label' : 'HYRJE',
        'return_amount_label' : 'KTHE KLIENTIT',
        'empty_ticket' : 'TIKETA ESHTE BOSH',
        'no_sufficient_money' : 'NUK KENI PARA TE MJAFTUESHME',
        'view_all_tickets' : 'SHIKO TIKETAT'
   };
/*
var items = [
		{name:'Coke', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Pepsi', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Fanta', category:'drink', sub_category:'non-alcoholic', price:2},
		{name:'Sprite', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Mineral Water', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Apple Juice', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Orange Juice', category:'drink', sub_category:'non-alcoholic', price:22},
		{name:'Lemonade', category:'drink', sub_category:'non-alcoholic', price:32},
		{name:'Machiato', category:'drink', sub_category:'coffee', price:2},
		{name:'Latte', category:'drink', sub_category:'coffee', price:2},
		{name:'Black Coffee', category:'drink', sub_category:'coffee', price:2},
		{name:'Capuchinno', category:'drink', sub_category:'coffee', price:2},
		{name:'Nescaffe', category:'drink', sub_category:'coffee', price:2},
		{name:'Apple Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Fruit Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Grapes Tea', category:'drink', sub_category:'tea', price:22},
		{name:'Chicken Burger', category:'food', sub_category:'fast-food', price:22},
		{name:'Hamburger', category:'food', sub_category:'fast-food', price:22},
		{name:'Chicken Cheese', category:'food', sub_category:'fast-food', price:22},
		{name:'French Fries', category:'food', sub_category:'fast-food', price:22}
	];
*/
var tables = [
        {nr:'1',available:true,waiter:"free", ticket:[],total:0},
        {nr:'2',available:true,waiter:"free", ticket:[],total:0},
        {nr:'3',available:true,waiter:"free", ticket:[],total:0},
        {nr:'4',available:true,waiter:"free", ticket:[],total:0},
        {nr:'5',available:true,waiter:"free", ticket:[],total:0},
        {nr:'6',available:true,waiter:"free", ticket:[],total:0},
        {nr:'7',available:true,waiter:"free", ticket:[],total:0},
        {nr:'8',available:true,waiter:"free", ticket:[],total:0},
        {nr:'9',available:true,waiter:"free", ticket:[],total:0},
        {nr:'10',available:true,waiter:"free", ticket:[],total:0},
        {nr:'11',available:true,waiter:"free", ticket:[],total:0},
        {nr:'12',available:true,waiter:"free", ticket:[],total:0}
     ];
/*
var waiters = [
        {name:'Kushtrim',password:'123',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Kreshnik',password:'456',logged_in:false,position:'Manager',profit:0,tables_served:0},
        {name:'Patriot',password:'789',logged_in:false,position:'Waiter',profit:0,tables_served:0},
        {name:'Nazif',password:'159',logged_in:false,position:'Waiter',profit:0,tables_served:0},
    ];
*/
