'use strict';
angular.module('EliteApps')
.factory('AllItems', ['$http', function($http){
	return{
		getItems : function() {
			return  $.get("/api/items",function() {})
        }
    }
}])
.factory('Strings', ['$http', function($http){
	return{
		getStrings : function() {
			return  $.get("/api/strings",function() {})
        }
    }
}])
.factory('Tables', ['$http', function($http){
	return{
		getTables : function() {
			return  $.get("/api/tables",function() {})
        }
    }
}])
.factory('Waiters', ['$http', function($http){
	return{
		getWaiters : function() {
			return  $.get("/api/waiters",function() {})
        }
    }
}])
/*
.factory('BusinessData', ['$http', function($http){
	return {
		getBusinessData : function() {
			return  $.get("/api/business");
        }
    }
	return {
		authenticateBusiness : function() {
			return  $.post("/api/authenticate_business");
		}
	}
}]) */
.factory('BusinessData', ['$http', function($http){
	var BusinessData = [];

	BusinessData.authenticate = function(data) {
		return $.post("/api/authenticate_business", data);
	};

	BusinessData.openTicket = function (data) {
		return $.post("/api/open_ticket", data);
	};

	BusinessData.addItemToTicket = function(data) {
		return $.post("/api/add_item_to_ticket", data);
	};

	BusinessData.changeItemTicketAmount = function(data) {
		return $.post("/api/change_item_ticket_amount", data);
	}

	BusinessData.closeTable = function(data) {
		return $.post("/api/close_table", data);
	}

	BusinessData.getAppStrings = function() {
		return $.get("/api/strings", function() {});
	};

	BusinessData.getSession = function() {
		return $.get("/api/session", function() {});
	};

	return BusinessData;
/*	return {
		getBusinessData1 : function() {
			return  $.get("/api/business");
        }
    }
	return {
		authenticateBusiness : function() {
			return  $.post("/api/authenticate_business");
		}
	} */
}])
.factory('', ['$http',function($http){
	Test.testme = function(name) {
		return 'welcome, '+name;
	};
}])
