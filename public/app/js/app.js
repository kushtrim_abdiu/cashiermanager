var app = angular.module('EliteApps', ['ui.router','ngMaterial','ngMdIcons','ngCookies'])
.config(function($urlRouterProvider,$stateProvider,$mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('teal')
    .accentPalette('red');

  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('app', {
        url:'/',
        templateUrl:'templates/app.html',
        controller: 'appController'
    })

});

app.controller('EliteAppsCtrl', ['$scope','$rootScope','$location', function($scope,$rootScope,$location) {

}]);
