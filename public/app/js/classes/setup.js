'use strict';
angular.module('EliteApps')
.factory('Setup', ['$http','$cookies', function($http,$cookies){

    var Setup = [];
    Setup.appStrings = {};
    Setup.appStringsLanguage;
    //GET THE APP STRINGS
    //get the app data
    Setup.requestStrings = function() {
        return $http.get("/api/strings");
    };

    Setup.requestStringsForLanguage = function(language) {
        return $http.post("/api/strings_for_language", {lang:language});
    }

    Setup.getStrings = function (response) {
        Setup.appStrings = response.data;
        Setup.appStringsLanguage = Setup.appStrings.language;
        //store them in a cookie
        var jsonStrings = angular.toJson(Setup.appStrings);
        $cookies.put('strings', jsonStrings);
    };

    Setup.getString = function(string_key) {
        return Setup.appStrings[string_key];
    }

    return Setup;
}]);
