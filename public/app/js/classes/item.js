'use strict';
angular.module('EliteApps')
.factory('Item', ['$http', function($http){
    var Item = [];
    Item.allItems;

    Item.setBusinessItems = function(items) {
        Item.allItems = items;
    };

    /* getSupCategoriesOfCategory - is called when we click on a category
     * after we have selected a table
     * @param (string) - category - the category for which we should get the subcategories
     * @return (array) - subCategories
     */
    Item.getSupCategoriesOfCategory = function(category) {
        var subCategories = [];
        Item.allItems.forEach(function(item,index,array) {
            if (item.category == category && subCategories.indexOf(item.sub_category) == -1) {
                subCategories.push(item.sub_category);
            }
        });

        return subCategories;
    }

    return Item;
}]);
