'use strict';
angular.module('EliteApps')
.factory('Waiter', ['$http','$rootScope', function($http,$rootScope){
    var Waiter = [];
    Waiter.allWaiters = {};
    Waiter.loginInput = "";
    Waiter.loggedWaiter = {};

    //when the app is initialized and the business is logged in we set up the Waiters array
    Waiter.setBusinessWaiters = function(waiters) {
        Waiter.initiateWaiters(waiters)
        Waiter.allWaiters = waiters;
    };

    Waiter.resetInput = function () {
        Waiter.loginInput = "";
    }
    //adds the necessery variables to the waiter object
    Waiter.initiateWaiters = function(waiters) {
        if (waiters != undefined) {
            waiters.forEach(function(waiter,index,array) {
                if (waiter['profit'] == undefined) {
                    waiter['profit'] = 0;
                }
                if (waiter['tables_served'] == undefined) {
                    waiter['tables_served'] = 0;
                }
            });

            return waiters;
        }
    }

    //this is triggered when the Waiter wants to login to open a ticket
    Waiter.updateWaiterInput = function(input) {
        //if backspace is not clicked
        if (input != -1) {
            Waiter.loginInput += input;
        }
        else {
            var new_input = "";
            for (var i = 0; i < Waiter.loginInput.length-1; i++) {
                new_input += Waiter.loginInput[i];
            }
            Waiter.loginInput = new_input;
        }

        return Waiter.loginInput;
    }

    //this function is triggered when the waiter has clicked login button
    Waiter.logWaiterIn = function(userInput) {
        //quick fix if we were to write the password by the keyboard not the keypad
        //then the loginInput remains empty
        Waiter.loginInput = userInput;
        Waiter.allWaiters.forEach(function(waiter,index,array) {
            //if we find the waiter with the password specified by the waiter input
            if (waiter.pin == Waiter.loginInput) {
                Waiter.loggedWaiter = waiter;
                Waiter.loggedWaiter.logged_in = true;
                //break; //we break because since we found the waiter we dont need to loop more
            }
        });
    }

    //returns true or false whether the user is succesfully logged in or not
    Waiter.isLoggedIn = function() {
        if (Waiter.loggedWaiter.id) {
            return true;
        }
        return false;
    }

    //log the current waiter out
    Waiter.logout = function() {
        Waiter.loggedWaiter = {};
        Waiter.loginInput = "";
    }

    //called when the waiter cliks on an item to add it to the ticket
    Waiter.tryAddItemToTicket = function(item_id,item_name,item_price,selected_table) {
        var ticketItem = {id:item_id,name:item_name,price:item_price,table:selected_table};
        return $http.post("/api/add_item_to_ticket", ticketItem);
    }
    /*
    Waiter.addItemToTicket = function(ticket) {
        console.log('this is the response from the server');
        console.log(ticket);
    } */

    //returns the logged waiter name
    Waiter.getName = function() {
        //console.log(Waiter.loggedWaiter.name);
        return Waiter.loggedWaiter.name;
    }

    //increase the counter for tables served
    //triggered when the waiter closes a table
    Waiter.tableServed = function(isClosed, sum) {
        for (var i = 0; i < Waiter.allWaiters.length; i++) {
			if (Waiter.allWaiters[i].name == $rootScope.currentWaiter.name) {
                Waiter.allWaiters[i].profit = parseFloat(Waiter.allWaiters[i].profit) + parseFloat(sum);
				//add +1 to the tables served
                if (isClosed) {
                    Waiter.allWaiters[i].tables_served = parseInt(Waiter.allWaiters[i].tables_served) + parseInt(1);
                }

                //store the profit and tables served of the waiter
				break;
			}
		}

        return Waiter.allWaiters;
    }

    //check if the user input is 4 digits long
    Waiter.inputIsValid = function(userInput) {
        //quick fix
        Waiter.loginInput = userInput;
        if (Waiter.loginInput.length == 4) {
            //convert it into int
            return true;
        }
        else {
            return false;
        }
    }

    Waiter.tryChangingPin = function() {
        return $http.post("/api/change_pin",{new_pin:Waiter.loginInput,waiter_id:$rootScope.currentWaiter.id});
    }


    return Waiter;
}]);
