'use strict';
angular.module('EliteApps')
.factory('Table', ['$http','$mdToast','$rootScope', function($http,$mdToast,$rootScope){
    var Table = [];
    Table.totalNrOfTables;
    Table.allTables = [];
    Table.hasTables = false;
    Table.currentselectedItem = -1;
    Table.hasUnprintedTickets = false;
    //this function is called at the begining when we initialize the app
    //to create the tables object
    //in the business object we have how many tables does this business have
    //so, that is the number of table objects we generate
    //we consider the fact that there might have been some tables in the session for some reason
    //and they are not closed so we loop through open tables to update their data
    // @param number (totalNrOfTables) - is the total nr of tables the business has
    // @param array (openedTables) - the tables that are still opened
    // @return void
    Table.setUpTables = function(totalNrOfTables,openedTables) {
        Table.totalNrOfTables = totalNrOfTables;
        //first we need to create a table object for every table
        //and if that table is in the opened tables we update it
        for (var i = 1; i <= totalNrOfTables; i++) {
            Table.allTables.push({nr : i, available : true, waiter : "free", ticket : [], total : 0});
        }
    }

    //this function is called when we load the app
    //it reads the data from the session and it gets the opened tickets
    Table.setUpTickets = function(tickets) {
        //console.log(Table.allTables);
        if (tickets != undefined && Object.keys(tickets).length > 0) {
            //if we have tickets we have to loop through them and set them up correctly
            Object.keys(tickets).forEach(function(key,index,array) {
                Table.updateTableTicket(tickets[key]);
            });
        }

        Table.hasTables = true;
        //return true;
        return Table.hasTables;
        //console.log(Table.allTables);
    }

    //a getter method to return the available tables
    Table.getAvailableTables = function() {
        //Table.allTables = tables;
        var availableTables = [];
        //here is the problem... it isnt updated with the freed tables
        Table.allTables.forEach(function(table,index,array) {
            if (table.available) {
                availableTables.push(table);
            }
        });

        return availableTables;
    }

    //this method is called when the worker clicks on a table to select
    Table.openTable = function(table) {
        //prepare the object ticket
        //first we have to send a request to the server to open a new table
        return $http.post("/api/open_ticket", table);
    }

    Table.takeTable = function(response) {
        Table.allTables.forEach(function(table,index,array) {
            if (table.nr == response.table) {
                //we SHOULD update the table waiter $rootScope.TABLE
                table.waiter = response.waiter;
                table.available = false;
            }
        });
    }

    Table.updateTableTicket = function(ticket) {
        if ($rootScope.TABLES != undefined) {
            //this is true when we add a new item
            //we have to think of a better solution because in the future
            //this function might not do what it is supposed to do
            var hasTheItem = false;
            Table.allTables[ticket.table - 1].ticket.forEach(function(item,index,array) {
                if (item.id == ticket.id) {
                    hasTheItem = true;
                    item.amount += 1;
                }
            });

            if (!hasTheItem) {
                //if the item doesnt exist in the ticket then we push it to the ticket
                //first we add some missing properties
                ticket.checked = true;
                ticket.amount = 1;
                //delete ticket.table;
                Table.allTables[ticket.table - 1].ticket.push(ticket);
            }

            Table.allTables[ticket.table - 1].total += ticket.price;
        }
        else {
            Table.allTables.forEach(function(table,index,array) {
                if (table.nr == ticket.table) {
                    //the table found we should update its ticket
                    table.ticket = ticket.items;
                    table.total = ticket.total_price;
                    table.available = false;
                    table.waiter = ticket.waiter;
                    //break;
                }
            });
        }

        return Table.allTables;

    }

    Table.tryRemoveItem = function(removeInformation) {
        return $http.post("/api/remove_item", removeInformation);
    }

    Table.removeItem = function(response) {
        $rootScope.TABLES[response.data.table - 1].ticket = response.data.items;
        $rootScope.TABLES[response.data.table - 1].total = response.data.total_price;
        $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.ticket_item_removed));
    }

   /* getTableSum - gets the total sum that needs to be payed, it takes it from the session
    * @param (int) : selected_table - is the table we want to close
    * @return (double) : theSum
    */
    Table.getTableSum = function(selected_table) {
        if (selected_table != 0) {
            return $http.post("/api/get_table_sum", {table_nr:selected_table});
        }
        else {
            $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.no_table_selected_msg));
        }
    }

   /* closeTable - called after the waiter pays the ticket from the client
    * @param (int) table_nr - is the table nr we are trying to close
    */
    Table.closeTable = function(table_nr) {
        return $http.post("/api/close_table", {table_nr:table_nr});
    }

    Table.handleCloseTable = function(response) {
        console.log('this is the response from the server');
        console.log(response.data);
        console.log('this is the response from the server');
        //we have some items unprinted
        //we were trying to split the ticket
        if (response.data.status == 'open') {
            //the ticket is still open
            Table.hasUnprintedTickets = true;
            $rootScope.TABLES[response.data.table_nr - 1].ticket = response.data.items;
            $rootScope.TABLES[response.data.table_nr - 1].total = response.data.total_price;
        }
        else {
            Table.hasUnprintedTickets = false;
        }
        /*
        Table.hasUnprintedTickets = true;
        $rootScope.TABLES.forEach(function(table,index,array) {
            if (table.nr == $rootScope.selected_table) {
                table.total = response.data;
                table.ticket = response.data;
                console.log('=================================');
                //console.log(table);
                console.log(response.data);
                console.log('=================================');
                //table.total = response.data.total_price;
                //delete response.data.total_price;
                //delete response.data.ticket_id;
                //table.ticket = response.data;
            }
        });*/
    }

    Table.waiterHasTables = function(waiter) {
        var hasTables = false;
        Table.allTables.forEach(function(table,index,array) {
            if (table.waiter == waiter) {
                hasTables = true
            }
        });

        return hasTables;
    };

    Table.tryRemoveTicket = function(table,waiter,msg) {
        return $http.post("/api/remove_ticket", {table_nr:table,waiter_id:waiter,message:msg});
    }

    Table.handleRemoveTicket = function(response) {
        if (response.data.status == 'success') {
            Table.releaseTable();
            // - 1 because the keys start from zero
            delete $rootScope.TABLES[$rootScope.selected_table-1];
            $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.ticket_canceled));
            $rootScope.selected_table = 0;
        }
        else {
            $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.ticket_canceled_failed));
        }
    }

    Table.releaseTable = function() {
        $rootScope.TABLES.forEach(function(table,index,array) {
            if (table.nr == $rootScope.selected_table) {
                table.waiter = 'free';
                table.available = true;
                table.ticket = [];
                table.total = 0;
                $rootScope.selected_table = 0;
                $rootScope.category_is_selected = false;
            }
        });

        $rootScope.has_tables = Table.waiterHasTables($rootScope.currentWaiter.name);
    }

    Table.tryToggleCheckItem = function(item_id) {
        Table.currentselectedItem = item_id;
        return $http.post("/api/toggle_item_checked", {item_id:item_id,table_nr:$rootScope.selected_table});
    }

    Table.handleToggleCheckedItem = function(response) {
        if (response.data.status == 'success') {
            //do something here maybe show a toast

        }
    }

    //if the server for some reason couldnt be reached or it didnt respond
    //and the toggle didnt happen in the server we revert back the change and show
    //a toast message saying changes didnt happen try again
    Table.handleFailToggleItem = function(response) {
        $rootScope.TABLES[$rootScope.selected_table - 1].ticket.forEach(function(item,index,ticket) {
            if (item.id == Table.currentselectedItem) {
                //the item in itself has been toggled so we didnt need to do this handling
                item.checked = !item.checked;
                $mdToast.show($mdToast.simple().content($rootScope.LOCAL_TEXTS.changes_failed));
            }
        });
    }

    Table.getOnlyTheSumOfSelectedItems = function() {
        var sum = 0;
        Table.allTables[$rootScope.selected_table-1].ticket.forEach(function(item,index,array) {
             if (item.checked) {
                 sum += item.price * item.amount;
                 //console.log(item);
             }
        });

        return sum;
    }


    Table.tryGetTicketsFromDate = function(date) {
        return $http.post("/api/all_tickets_from_date", {date:date});
    }

    Table.handleGetTicketsFromDate = function(response) {
        console.log('these are the tickets from the specified date');
        console.log(response);
        console.log('these are the tickets specified from date');
    }

    return Table;
}]);
