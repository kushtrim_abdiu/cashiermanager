'use strict';
angular.module('EliteApps')
.factory('Business', ['$http', function($http){
    var Business = [];
    Business.isLoggedIn = false;
    Business.details = {};

    //make a request for the session data
    Business.requestSession = function() {
         return $http.get("/api/session");
    }

    //check the response and see if there is a session or not
    Business.getSession = function(response) {
        if (response.data.status == 'success') {
            Business.isLoggedIn = true;
            Business.details = response.data;
        }

        return response;
    }

    //called when the client clicks login as a business
    //sends the request with the user input (credentials) to authenticate business
    Business.tryAuthenticate = function (credentials) {
        return $http.post("/api/authenticate_business", credentials);
    };

    //handles the response to the authentication request
    Business.authenticationResponse = function(response) {
        if (response.data.status == 'success') {
            Business.isLoggedIn = true;
            Business.details = response.data.business;
        }

        return response;
    };

    //this function is called when we want to create a new ticket - i.e. new table
    //it returns only those tables that are free
    Business.getAvailableTables = function() {
        console.log(Business.details);
    }

    return Business;
}]);
