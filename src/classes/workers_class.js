'use strict';

//var Workers = require('../mongo/models/workers');
var Workers = require('../models').workers;

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'qTLw4k+H',
  database : 'elite_manager'
});

connection.connect();

var E_Workers = [];

E_Workers.allWorkers = [];

E_Workers.getWorkersOfBusiness = function(business_id) {
    return new Promise(function(resolve,reject) {
        var bid = business_id; //make sure the bid is  sanitized
        var queryWorkers = "SELECT id,name,surname,role,pin FROM workers "+
            "WHERE BusinessId=?";
        connection.query(queryWorkers,[bid],function(err,rows,fields) {
            if (!err) {
                var workersArray = [];
                rows.forEach(function(worker,index,array) {
                    workersArray.push(worker);
                });
                var workersHash = {};
                workersHash.workers = workersArray;
                resolve(workersHash);
            }
        });
    });
};

E_Workers.pinExists = function(pinInfo) {
    var newPin = pinInfo.new_pin;
    var waiterID = pinInfo.waiter_id;
    return new Promise(function(resolve,reject) {
        var query = "SELECT id FROM workers WHERE pin = ?";
        connection.query(query,[newPin,waiterID],function(err,rows,fields) {
            if (!err) {
                if (rows.length < 1) {
                    resolve({status:'not_exist'})
                }
                else if(rows.length == 1) {
                    if (rows[0].id == waiterID) {
                        //there is a bug in the mysql for nodejs
                        //so we have to do it this way instead of saying
                        //in the query that id<> (not equal to the users id)
                        resolve({status:'not_exist'});
                    }
                    else {
                        resolve({status:'exist'});
                    }
                }
                else {
                    reject({status:'reject'});
                }
            }
            else {
                reject({error:"something went wrong"});
            }
        });
    });
}

E_Workers.updateWorkerPin = function(pinInfo) {
    var newPin = pinInfo.new_pin;
    var waiterID = pinInfo.waiter_id;
    return new Promise(function(resolve,reject) {
        var queryUpdatePin = "UPDATE workers SET pin=? WHERE id=?";
        connection.query(queryUpdatePin,[newPin,waiterID],function(err,rows,fields) {
            if (rows.changedRows == 1) {
                //it was successful
                resolve({status:'success'});
            }
            else {
                reject({status:'fail'});
            }
        });
    });

};

module.exports = E_Workers;
