'use strict';

//var Items = require('../mongo/models/item');
var Promise = require('bluebird');
var Items = require('../models').items;
var Categories = require('../models').categories;
var Sub_Categories = require('../models').sub_categories;

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'qTLw4k+H',
  database : 'elite_manager'
  //socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
});

connection.connect();

var E_Items = [];

E_Items.getItemsOfBusiness = function(business_id) {
    return new Promise(function(resolve,reject) {
        var bid = business_id; //make sure the bid is  sanitized
        var queryItems = "SELECT i.id,i.name,i.sell_price AS price,i.unit, "+
            "c.name AS category,s.name AS sub_category "+
            "FROM items i,categories c, sub_categories s "+
            "WHERE i.BusinessId=? AND c.id=i.categoryId AND s.id=i.subCategoryId";

        connection.query(queryItems,[bid], function(err,rows,fields) {
            if (!err) {
                var itemsArray = [];
                rows.forEach(function(item,index,array) {
                    itemsArray.push(item);
                });
                var itemsHash = {};
                itemsHash.items = itemsArray;
                resolve(itemsHash);
            }
            else {
                reject({error:"Something went wrong"});
            }
        });
    });
};

//connection.end();

module.exports = E_Items;
