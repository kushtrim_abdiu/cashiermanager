'use strict';

var Business = require('../mongo/models/business');
var Promise = require('bluebird');
var Businesses = require('../models').Businesses;
var Categories = require('../models').categories;

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'qTLw4k+H',
  database : 'elite_manager'
  //socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
});

connection.connect();

var E_Business = [];

E_Business.tryAuthenticate = function(usrname,pwd) {
    return new Promise(function(resolve,reject) {
        //console.log('username::'+username+" password:"+password);
        var username = usrname; //we should sanitize them to prevent sql injections
        var password = pwd; //sanitize it for sql injection protection

        var query = "SELECT id,name,username,nr_tables,language " +
            "FROM Businesses WHERE username = ? AND password = ?";
        connection.query(query,[username,password], function(err,rows,fields) {
            if (!err) {
                //if query was executed successfully
                var result = rows[0];
                resolve(rows[0]);
            }
            else {
                reject(err);
            }
        });
    });

};

/*
E_Business.tryAuthenticate = function(username,password) {
    return new Promise(function(resolve,reject) {
        Businesses.findAll({where:{username:username,password:password}})
        .then(function(business) {
             resolve(business[0].dataValues);
        });
    });
}

//gets all the categories of the business
E_Business.getBusinessCategories = function(business_id) {
    return new Promise(function(resolve,reject) {
        Categories.findAll({where:{business_id:business_id}})
        .then(function(categories) {
            resolve(categories);
        });
    });
}
*/
/*
function E_Business () {
    this.business;
}

E_Business.prototype.authenticateBusiness = function(username,password,callback) {
    Business.find({"username":username,"password":password},
        {username:true,nr_tables:true,email:true,manager:true},function (err,business){
        if (err) {
            console.log("username:" + username + " password:"+password + err.message);
            callback({},'fail');
            return res.status(500).json({message: err.message});
        }
        else if (business.length == 1) {
            //if the length is one then its correctly identified if its bigger then something is wrong
            this.business = business[0];
            callback(this.business,'success');
        }
        else {
            //the username or password is wrong
            callback({},'fail');
        }
    });
}

E_Business.prototype.getBusinessData = function() {
    return this.business;
}

E_Business.prototype.getBusinessID = function() {
    return this.business._id;
}

E_Business.prototype.getBusinessInfo = function() {
    return {
        corp_id : this.business.corp_id,
        manager : this.business.manager,
        username : this.business.username,
        nr_tables : this.business.nr_tables
    }
}

*/

module.exports = E_Business;
