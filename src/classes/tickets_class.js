'use strict';

var Promise = require('bluebird');
var Tickets = require('../models').tickets;

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'qTLw4k+H',
  database : 'elite_manager'
  //socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
});

connection.connect();

var E_Tickets = [];
//used when we want to print the ticket and close it
//if there is any item that is not checked we shouldnt remove the list from the session
//we only remove the items checked
E_Tickets.hasUncheckedItems = false;
E_Tickets.uncheckedItems = [];
//the total price of the checked items
E_Tickets.totalPriceOfCheckedItems = 0;

//works
E_Tickets.saveTicket = function(business_id,ticket) {
    return new Promise(function(resolve,reject) {
        //var open_time_stamp = E_Tickets.getMySQLTimeStampFormat(ticket.time_stamp);
        var openDate = new Date();
        //openDate.setTime(ticket.time_stamp*1000);
        //var dateString = openDate.toUTCString();
        var dateString = openDate;
        //console.log('this is the date string ::'+dateString); */
        var queryTicket = "INSERT INTO tickets(BusinessId,worker_id,clients,opened_at,total) VALUES(?,?,?,?,?)";
            var theQuery = connection.query(queryTicket,[business_id,ticket.waiter_id,ticket.clients,dateString,
                E_Tickets.totalPriceOfCheckedItems], function(err,result) {
                if (!err) {
                    //console.log('this is the id : '+result.insertId);
                    resolve({ticket_id:result.insertId});
                }
                else {
                    reject({status:"fail"});
                }
            });
    });
};

//NEW
//this function closes the ticket with the specified ID
E_Tickets.closeTicket = function(ticketID)
{
    var closeTicket = new Promise(function(resolve,reject) {
        var query = "UPDATE tickets set closed_at=? WHERE id=?";
        var execute = connection.query(query,[Date.now(),ticketID], function(err,result) {
            if (!err) {
                resolve({status:"success"});
            }
            else reject({status:"fail"});
        });
    });

    closeTicket.then(function(response) {
        return response;
    });
}

//NEW::
//accepts an array of items and the ticket id
//we loop through the array and we insert it into the db
E_Tickets.updateItemsStatus = function(ticketID, items) {
    return new Promise(function(resolve,reject) {
        items.forEach(function(item,index,array) {
            return new Promise(function(res,rej) {
                 var query = "INSERT INTO ticket_items(ticketId,item_id,amount,price,status) VALUES(?,?,?,?,?)";
                 var execute = connection.query(query,[ticketID,item.id,item.amount,item.price,1], function(err,result) {
                     if (!err) {
                         res({id:result.insertId});
                     }
                     else {
                         rej({status:'fail'});
                     }
                 });
            });
        });

    });
};

//maybe to be replaced
E_Tickets.saveTicketRows = function(ticket_id,items) {
     var saveRows = new Promise(function(resolve,reject) {
        items.forEach(function(item,index,array) {
            //we insert each item into ticket_items
            var query = "INSERT INTO ticket_items(ticketId,item_id,amount,price,status) VALUES(?,?,?,?,?)";
            connection.query(query,[ticket_id,item.id,item.amount,item.price,1], function(err,result) {
            });
        });
    });
    saveRows.then(function() {
        return {status:'success'};
    });
};

E_Tickets.getItemsChecked = function(items) {
    var newTicket = [];
    E_Tickets.uncheckedItems = [];
    E_Tickets.hasUncheckedItems = false;
    items.forEach(function(item,index,array) {
        if (item.checked == true) {
            newTicket.push(item);
            E_Tickets.totalPriceOfCheckedItems += item.price;
        }
        else {
            E_Tickets.uncheckedItems.push(item);
            E_Tickets.hasUncheckedItems = true;
        }
    });

    return newTicket;
}

E_Tickets.totalPriceOfUnCheckedItems = function() {
    var sum = 0;
    E_Tickets.uncheckedItems.forEach(function(item,index,array) {
        sum += item.amount * item.price;
    });

    return sum;
}

//THIS FUNCTION IS ALMOST SIMILAR TO saveTicket function
//maybe in the future we merge them
E_Tickets.cancelTicket = function(business_id,ticket,message) {
    return new Promise(function(resolve,reject) {
        //var open_time_stamp = E_Tickets.getMySQLTimeStampFormat(ticket.time_stamp);
        var openDate = new Date();
        /*openDate.setTime(ticket.time_stamp*1000); */
        //var dateString = openDate.toUTCString();
        var dateString = openDate;
        //console.log('this is the date string ::'+dateString); */
        var queryTicket = "INSERT INTO tickets(BusinessId,worker_id,opened_at,total,canceled,cancel_message) VALUES(?,?,?,?,?,?)";
            var current_timestamp = Date.now();
            var theQuery = connection.query(queryTicket,[business_id,ticket.waiter_id,dateString,
                ticket.total_price,true,message], function(err,result) {
                if (!err) {
                    //console.log('this is the id : '+result.insertId);
                    resolve({ticket_id:result.insertId});
                }
                else {
                    reject({status:"fail"});
                }
            });
    });
}

E_Tickets.getTicketForDate = function(date,business_id)
{
    return new Promise(function(resolve,reject) {
        console.log('this is business id :' + business_id);

        var query = "SELECT * FROM tickets WHERE BusinessId = ? AND closed_at <= CURDATE() LIMIT 30";
        var execute = connection.query(query,[business_id], function(err,result) {
            if (!err) {
                //resolve({status:"success"});
                console.log('successs....................');
                console.log(result);
                console.log('successs....................');
            }
            else {
                console.log('fail');
                //reject({status:"fail"});
            }
        });

    });
}

E_Tickets.getMySQLTimeStampFormat = function(unix_time) {
    var openDate = new Date(unix_time);
    var value = openDate.getTime();
    /*
    openDate.setTime(unix_time*1000);
    var dateString = openDate.toUTCString(); */
    console.log(value);
};


module.exports = E_Tickets;
