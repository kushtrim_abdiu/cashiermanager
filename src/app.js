'use strict';

var express = require('express');
var redis   = require("redis");
var cookieParser = require('cookie-parser');
var session = require('express-session');
var sequelize = require('./models').sequelize;
var RedisStore = require('connect-redis')(session);
var parser = require('body-parser');
var client  = redis.createClient();
var app = express();
app.use(cookieParser());

var api = require('./api/index');
var mongoose = require('./mongo/database.js');
require('./mongo/seed.js');

app.use(session({
    secret: 'eliteappsecretkey42',
    store: new RedisStore({host:'localhost',port:6379,client:client,ttl:269990,rolling:true}),
    saveUninitialized: false,
    resave: false
}));

//parser
app.use(parser.json());
app.use(parser.urlencoded({extended : false}));

//static contents
app.use('/',express.static(__dirname + '/../public/landing_page/'));
app.use('/app',express.static(__dirname + '/../public/app'));

app.use('/api', api);

sequelize.sync().then(function(){
    //set the server to listen to the specified port
    app.listen('4242', function() {
        console.log('server is running right here');
    });
});
