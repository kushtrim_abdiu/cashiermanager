'use strict';

//the router of our app
var express = require('express');
var path = require('path');
var router = express.Router();
var app = express();

//app.use('/', express.static(__dirname + '/../public'));
app.use('/', express.static(path.join(__dirname, 'public')));


// export your router
module.exports = router;
