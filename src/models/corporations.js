'use strict';
module.exports = function(sequelize, DataTypes) {
  var corporations = sequelize.define('corporations', {
    corporation: DataTypes.STRING,
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    state: DataTypes.STRING,
    city: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        corporations.hasMany(models.Businesses/*,{foreignKey:'business_id'}*/);
      }
    }
  });
  return corporations;
};
