'use strict';
module.exports = function(sequelize, DataTypes) {
  var tickets = sequelize.define('tickets', {
    //BusinessId: DataTypes.INTEGER,
    worker_id: DataTypes.INTEGER,
    opened_at: DataTypes.DATE,
    closed_at: DataTypes.DATE,
    total: DataTypes.DOUBLE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        tickets.hasMany(models.ticket_items/*,{foreignKey:'category_id'}*/);
        tickets.belongsTo(models.Businesses/*,{foreignKey:'business_id'}*/);
      }
    }
  });
  return tickets;
};
