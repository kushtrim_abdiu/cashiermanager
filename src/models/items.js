'use strict';

module.exports = function(sequelize, DataTypes) {
  var items = sequelize.define('items', {
    //business_id: DataTypes.INTEGER,
    //category_id: DataTypes.INTEGER,
    //sub_category_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    distributor: DataTypes.STRING,
    buy_price: DataTypes.DOUBLE,
    sell_price: DataTypes.DOUBLE,
    unit: DataTypes.STRING,
    amount: DataTypes.DOUBLE,
    available: DataTypes.BOOLEAN,
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        items.belongsTo(models.Businesses/*,{foreignKey:'business_id'}*/);
        items.belongsTo(models.categories/*,{foreignKey:'category_id'}*/);
        items.belongsTo(models.sub_categories/*,{foreignKey:'sub_category_id'}*/);
      }
    }
  });

  return items;
};
