'use strict';
module.exports = function(sequelize, DataTypes) {
  var Businesses = sequelize.define('Businesses', {
    //corporation_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    state: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Businesses.hasMany(models.categories/*,{foreignKey:'category_id'}*/);
        //Businesses.hasMany(models.sub_categories/*,{foreignKey:'sub_category_id'}*/);
        Businesses.hasMany(models.items/*,{foreignKey:'item_id'}*/);
        Businesses.hasMany(models.workers/*,{foreignKey:'worker_id'}*/);
        Businesses.hasMany(models.tickets/*,{foreignKey:'ticket_id'}*/);
        Businesses.belongsTo(models.corporations/*,{foreignKey:'corporation_id'}*/);
      }
    }
  });

  return Businesses;
};
