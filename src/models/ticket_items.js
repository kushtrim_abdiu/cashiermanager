'use strict';
module.exports = function(sequelize, DataTypes) {
  var ticket_items = sequelize.define('ticket_items', {
    //ticket_id: DataTypes.INTEGER,
    item_id: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    price: DataTypes.DOUBLE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        ticket_items.belongsTo(models.tickets);
      }
    }
  });
  return ticket_items;
};
