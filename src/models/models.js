'use strict';

//CORPORATION MODEL
module.exports = function(sequelize, DataTypes) {
  var corporations = sequelize.define('corporations', {
    corporation: DataTypes.STRING,
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    state: DataTypes.STRING,
    city: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return corporations;
};

//BUSINESS MODEL
module.exports = function(sequelize, DataTypes) {
  var Businesses = sequelize.define('Businesses', {
    corporation_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    state: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  
  return Businesses;
};

//CATEGORY MODEL
module.exports = function(sequelize, DataTypes) {
  var categories = sequelize.define('categories', {
    business_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  categories.belongsTo(items, {foreignKey: 'user_id'})
  return categories;
};

//ITEMS MODEL
module.exports = function(sequelize, DataTypes) {
  var items = sequelize.define('items', {
    business_id: DataTypes.INTEGER,
    category_id: DataTypes.INTEGER,
    sub_category_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    distributor: DataTypes.STRING,
    buy_price: DataTypes.DOUBLE,
    sell_price: DataTypes.DOUBLE,
    unit: DataTypes.STRING,
    amount: DataTypes.DOUBLE,
    available: DataTypes.BOOLEAN,
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });

  items.hasMany(categories, {foreignKey: 'category_id'});
  //items.hasMany(sub_categories,{foreignKey: 'sub_category_id'});

  return items;
};

//SUB CATEGORIES MODEL
module.exports = function(sequelize, DataTypes) {
  var sub_categories = sequelize.define('sub_categories', {
    category_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return sub_categories;
};

//TICKET MODEL
module.exports = function(sequelize, DataTypes) {
  var tickets = sequelize.define('tickets', {
    business_id: DataTypes.INTEGER,
    worker_id: DataTypes.INTEGER,
    opened_at: DataTypes.DATE,
    closed_at: DataTypes.DATE,
    total: DataTypes.DOUBLE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return tickets;
};

//TICKET ITEMS MODEL
module.exports = function(sequelize, DataTypes) {
  var ticket_items = sequelize.define('ticket_items', {
    ticket_id: DataTypes.INTEGER,
    item_id: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    price: DataTypes.DOUBLE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return ticket_items;
};

//WORKERS MODEL
module.exports = function(sequelize, DataTypes) {
  var workers = sequelize.define('workers', {
    business_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    role: DataTypes.STRING,
    start_date: DataTypes.DATE,
    payment_type: DataTypes.BOOLEAN,
    salary: DataTypes.DOUBLE,
    pin: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return workers;
};
