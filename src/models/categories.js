'use strict';

module.exports = function(sequelize, DataTypes) {
  var categories = sequelize.define('categories', {
    //business_d: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        categories.belongsTo(models.Businesses/*,{foreignKey:'business_id'}*/);
      }
    }
  });
  return categories;
};
