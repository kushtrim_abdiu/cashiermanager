'use strict';
module.exports = function(sequelize, DataTypes) {
  var workers = sequelize.define('workers', {
    //business_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    role: DataTypes.STRING,
    start_date: DataTypes.DATE,
    payment_type: DataTypes.BOOLEAN,
    salary: DataTypes.DOUBLE,
    pin: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        workers.belongsTo(models.Businesses/*,{foreignKey:'business_id'}*/);
      }
    }
  });
  return workers;
};
