'use strict';
module.exports = function(sequelize, DataTypes) {
  var sub_categories = sequelize.define('sub_categories', {
    //category_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        sub_categories.belongsTo(models.categories/*,{foreignKey:'category_id'}*/);
      }
    }
  });
  return sub_categories;
};
