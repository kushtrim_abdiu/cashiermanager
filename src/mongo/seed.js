'use strict';

var Workers = require('./models/workers.js');
var Corporation = require('./models/corporation.js');
var Business = require('./models/business.js');
var Items = require('./models/item.js');
var Corporations = require('./models/corporations.js');

var workers = [
    {"business_id":"5738d42d760851da5116ee7f","name":"Kushtrim","password":"123","logged_in":false,
        "role":"waiter","start_date":new Date().toISOString(),"payment_type":0,"payment":300},
    {"business_id":"5738d42d760851da5116ee7f","name":"Kreshnik","password":"345","logged_in":false,
        "role":"manager","start_date":new Date().toISOString(),"payment_type":0,"payment":300},
    {"business_id":"5738d42d760851da5116ee7f","name":"Patriot","password":"567","logged_in":false,
        "role":"waiter","start_date":new Date().toISOString(),"payment_type":0,"payment":300},
    {"business_id":"5738d42d760851da5116ee7f","name":"Nazif","password":"789","logged_in":false,
        "role":"waiter","start_date":new Date().toISOString(),"payment_type":0,"payment":300},
];

var businesses = [
    {"corp_id":"571408b5e559edec398ec9b2","name":"Elite Bar","manager":"Kushtrim","phone":"070941090",
    "email":"kushtrim.abdiu.16@gmail.com","state":"Macedonia","city":"Struga","nr_tables":20,
    "username":"elitebar","password":"123456","registered_at":new Date().toISOString()},
    {"corp_id":"571408b5e559edec398ec9b2","name":"Elite Bar2","manager":"Kreshnik","phone":"070941090",
    "email":"kreshnik.abdiu@gmail.com","state":"Macedonia","city":"Struga","nr_tables":20,
    "username":"elitebar2","password":"123456","registered_at":new Date().toISOString()},
];

var corporation = [
    {"corporation":"Kushtrim AB","owner":"Kushtrim Abdiu","phone":"070941090",
    "email":"kushtrim.abdiu.16@gmail.com","state":"Sweden","city":"Vaxjo",
    "username":"kushtrim","password":"123456"}
];

var items = [
    {"business_id":"5738d42d760851da5116ee7f","name":"Coke", "category":"drinks", "sub_category":"non-alcoholic","buy_price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Pepsi", "category":"drinks", "sub_category":"non-alcoholic", "price":32,"sell_price":40,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Fanta", "category":"drinks", "sub_category":"non-alcoholic", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Sprite", "category":"drinks", "sub_category":"non-alcoholic", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Mineral Water", "category":"drinks", "sub_category":"non-alcoholic", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Apple Juice", "category":"drinks", "sub_category":"non-alcoholic", "price":32,"sell_price":34,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Orange Juice", "category":"drinks", "sub_category":"non-alcoholic", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Lemonade", "category":"drinks", "sub_category":"non-alcoholic", "price":32,"sell_price":38,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Machiato", "category":"drinks", "sub_category":"coffee", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Latte", "category":"drinks", "sub_category":"coffee", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Black Coffee", "category":"drinks", "sub_category":"coffee", "price":2,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Capuchinno", "category":"drinks", "sub_category":"coffee", "price":2,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Nescaffe", "category":"drinks", "sub_category":"coffee", "price":2,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Apple Tea", "category":"drinks", "sub_category":"tea", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Fruit Tea", "category":"drinks", "sub_category":"tea", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Grapes Tea", "category":"drinks", "sub_category":"tea", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Chicken Burger", "category":"food", "sub_category":"fast-food", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Hamburger", "category":"food", "sub_category":"fast-food", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Chicken Cheese", "category":"food", "sub_category":"fast-food", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"French Fries", "category":"food", "sub_category":"fast-food", "price":22,"sell_price":30,"available":true,"amount":100,"unit":"unit"},
    {"business_id":"5738d42d760851da5116ee7f","name":"Pizza", "category":"food", "sub_category":"fast-food", "price":32,"sell_price":30,"available":true,"amount":100,"unit":"unit"}
];

var corporations = [
    {
        "corporation" : "Elite Corporation",
        "owner" : "Kushtrim Abdiu",
        "phone" : "070941090",
        "email" : "kushtrim.abdiu.16@gmail.com",
        "state" : "Macedonia",
        "city" : "Struga",
        "username" : "kushtrim",
        "password" : "123456",
        "registered_at" : new Date().toISOString(),
        "businesses" : [
            {
            "business_id" : "5738d42d760851da5116ee7f",
            "name" : "Elite Bar",
            "phone" : "07093212",
            "modules" : {"basic":true,"online_ordering":true,"sms_notification":true,"email_notification":true},
            "username" : "aquarius_beach",
            "password" : "123456",
            "workers" : [
                {"name":"Kushtrim","password":"123","logged_in":false,
                    "role":"waiter","start_date":new Date().toISOString(),"payment_type":0,"payment":300},
                {"name":"Kreshnik","password":"345","logged_in":false,
                    "role":"manager","start_date":new Date().toISOString(),"payment_type":0,"payment":300},
                {"name":"Patriot","password":"567","logged_in":false,
                    "role":"waiter","start_date":new Date().toISOString(),"payment_type":0,"payment":300},
                {"name":"Nazif","password":"789","logged_in":false,
                    "role":"waiter","start_date":new Date().toISOString(),"payment_type":0,"payment":300}
                ]
            }
        ]
    }
];


Workers.remove({},function(err,status){});
//Business.remove({},function(err,status){});
workers.forEach(insertWorkers);
//businesses.forEach(insertBusinesses);

function insertWorkers(element,index,array) {
   //Workers.find();
   Workers.create({
        business_id : element['business_id'],
        name : element['name'],
        password : element['password'],
        logged_in : element['logged_in'],
        role : element['role'],
        start_date : element['start_date'],
        payment_type : element['payment_type'],
        payment : element['payment']
    },function(err,w){if (err) {console.log(err);}});
}

Items.remove({},function(err,status){});
items.forEach(insertItems);

function insertItems(element,index,array) {
    Items.create({
        business_id : element['business_id'],
        name : element['name'],
        category : element['category'],
        sub_category : element['sub_category'],
        buy_price : element['buy_price'],
        sell_price : element['sell_price'],
        available : element['available'],
        amount : element['amount'],
        unit : element['unit']
    },function(err,w){if (err) {console.log(err);}});
}
/*
function insertBusinesses(element,index,array) {
    Business.create({
        corp_id : element['corp_id'],
        name : element['name'],
        manager : element['manager'],
        phone : element['phone'],
        email : element['email'],
        state : element['state'],
        city : element['city'],
        nr_tables : element['nr_tables'],
        username : element['username'],
        password : element['password'],
        registered_at : element['registered_at']
    },function(err,w){if (err) {console.log(err);}});
}*/

Corporation.find({"corporation":"Kushtrim AB"}, function(err,corp){
    if (!err && !corp.length) {
        Corporation.create({
            corporation : "Kushtrim AB",
            owner : "Kushtrim Abdiu",
            phone : "070941090",
            email : "kushtrim.abdiu.16@gmail.com",
            state : "Sweden",
            city : "Vaxjo",
            username : "kushtrim",
            password : "123456"
        });
    }
});
