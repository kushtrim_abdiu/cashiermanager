'use strict';

var mongoose = require('mongoose');

var ticketSchema = new mongoose.Schema({
    business_id : Number,
    waiter_id : Number,
    open_time_stamp : Date,
    close_time_stamp : Date,
    items : mongoose.Schema.Types.Mixed,
    total : Number,
    table_nr : Number,
    net_profit : Number
});

var modelTicket = mongoose.model('Tickets',ticketSchema);

module.exports = modelTicket;
