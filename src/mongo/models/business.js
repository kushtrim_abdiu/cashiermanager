'use strict';

var mongoose = require('mongoose');

var businessSchema = new mongoose.Schema({
    corp_id : String,
    manager : String,
    phone : String,
    email : String,
    state : String,
    city : String,
    nr_tables : Number,
    username : String,
    password : String,
    registered_at : Date
});

var modelBusiness = mongoose.model('Business',businessSchema);

module.exports = modelBusiness;
