'use strict';

var mongoose = require('mongoose');

var corporationSchema = new mongoose.Schema({
    corporation : String, /* This is the corporation name*/
    owner : String,
    phone : String,
    email : String,
    state : String,
    city : String,
    username : String,
    password : String,
    registered_at : Date
});

var modelCorporation = mongoose.model('Corporation',corporationSchema);

module.exports = modelCorporation;
