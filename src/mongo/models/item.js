'use strict';

var mongoose = require('mongoose');

var itemSchema = new mongoose.Schema({
    business_id : String,
    name : String,
    category : String,
    sub_category : String,
    buy_price : Number,
    sell_price : Number,
    available : Boolean,
    amount : Number,
    unit : String
});

var modelItem = mongoose.model('Item',itemSchema);

module.exports = modelItem;
