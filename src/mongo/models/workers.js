'use strict';
//***mongoose is Singleton ***

var mongoose = require('mongoose');

//worker name, password, logged_in,position,profit,tables_served

var waitersSchema = new mongoose.Schema({
    business_id : String,
    name : String,
    password : String,
    logged_in : Boolean,
    role : String,
    start_date : Date,
    payment_type : {type : Number, min : 0, max : 1},
    payment : Number,
});

var modelWorker = mongoose.model('Workers',waitersSchema);

module.exports = modelWorker;
