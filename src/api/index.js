'use strict';

var express = require('express');
var router = express.Router();
var Promise = require('bluebird');
var Businesses = require('../models').Businesses;

//first we should prepare an array with the business data - TO BE REMOVED
var strings = require('../mock/strings.json');
var waiters = require('../mock/waiters.json');
var tables = require('../mock/tables.json');
var items = require('../mock/items.json');

//OBJECT MODELS - TO BE REMOVED
var Workers = require('../mongo/models/workers');
var Corporation = require('../mongo/models/corporation');
var Business = require('../mongo/models/business');
var Items = require('../mongo/models/item');

//classes - e_ stands for elite i.e. our class
var E_Business = require('../classes/business_class');
var E_Workers = require('../classes/workers_class');
var E_Items = require('../classes/items_class');
var E_Tickets = require('../classes/tickets_class');

//API-S FOR THE CORE APPLICATION
router.get('/strings', function(req,res,next) {
    var localStrings = {};
    if (req.session.business != undefined && req.session.business.length > 1) {
        //if there is a session get the language settings
        localStrings = req.session.business.language;
    }
    else {
        //the default language is english
        localStrings = strings.eng;
    }

    res.json(localStrings);
});

//called when we want strings from a specific language
router.post('/strings_for_language', function(req,res,next) {
    var language = req.body.lang;

    res.json(strings[language]);
});

/** the code that handles the authentication is kind of messy
 * because we basically do three things in this function
 * firstly authenticate and then based on that response which is asynchronous
 * we get the workers and the items in a messy way because thats how javascript is
 * WE SHOULD THINK FOR A BETTER DESIGN
 */
router.post('/authenticate_business', function(req,res,next) {
    //we should authenticate the business and if it is correct we should set the session
    var usrname = req.body.username; //we have to sanitize the input
    var pwd = req.body.password; //we have to sanitize the input
    var response = {};
    //authenticate business
    var workers;
    var items;

    //try to authenticate the business
    var business = new Promise(function(resolve,reject) {
        var auth_response = E_Business.tryAuthenticate(usrname,pwd);
        return resolve(auth_response);
    });
    //then read the authentication response
    business.then(function(business) {
        if (business && business.id) {
            //the login was succesful
            //we have to start creating the business dictionary
            ///get the workers of the business
            workers = new Promise(function(resolve,reject) {
                var workers_response = E_Workers.getWorkersOfBusiness(business.id);
                return resolve(workers_response);
            });
            //get the business items
            items = new Promise(function(resolve, reject) {
                var items_response = E_Items.getItemsOfBusiness(business.id);
                return resolve(items_response);
            });
            //when both the items and the workers are retrieved
            Promise.all([items,workers]).then(function(values) {
                //NEEDS REIMPLEMENTATION
                var allData = {};
                allData.status = 'success';
                allData.business = business;
                //we have the data but they are not in the correct format
                //for that we will have to restrcture the dictionary/hash
                values.forEach(function(hash,index,array) {
                    if (hash.items) {
                        allData.items = hash.items;
                    }
                    else if (hash.workers) {
                        allData.workers = hash.workers;
                    }
                });
                //store the hash in our session
                req.session.business = allData;
                //console.log(req.session.business);

                res.json(req.session.business);
            });

        }
        else {
            console.log('no business');
        }
    });
});

//check if there is session, if there is session then return it to the client
router.get('/session', function(req,res,next) {
    //req.session.business = []; //reset session when needed
    if (req.session.business != undefined) {
        //console.log(req.session.business);
        res.json(req.session.business);
    }
    else {
        res.json({'status' : 'no_session'});
    }
});

router.post('/open_ticket', function(req,res,next) {
    var business_id = req.session.business.business.id;
    var ticket = {
        "waiter_id"   : req.body.waiter_id,
        "waiter"      : req.body.name,
        "table"       : req.body.table,
        "time_stamp"  : Date.now(),
        "items"       : [],
        "total_price" : 0,
        "clients"     : req.body.people,
        "printed"     : false
    };
    //console.log(ticket);
    //if the tickets array is not set in the session initiate it
    if (req.session.business.tickets == undefined) {
        req.session.business.tickets = {};
    }

    //first we have to add the ticket to the database
    //and get the ticket id
    var new_ticket = new Promise(function(resolve,reject) {
        var store_ticket = E_Tickets.saveTicket(business_id,ticket);
        return resolve(store_ticket);
    });

    new_ticket.then(function(response) {
        ticket.db_id = response.ticket_id;
        //we add the ticket to the session
        //we keep all the open tickets in the session until they are closed
        //req.session.business.tickets.push(ticket);
        req.session.business.tickets[req.body.table] = ticket;

        //console.log(req.session.business.tickets);
        res.json(req.session.business.tickets[req.body.table]);
    });
    /*
    var new_ticket = {"waiter_id" : req.body.waiter_id, "waiter" : req.body.name,
        "table" : req.body.table, "time_stamp" : Date.now(), "items" : [], "total_price":0};
    //req.session.business.tickets = {};
    if (req.session.business.tickets == undefined) {
        //there is no tickets in the session so we create the ticket object
        req.session.business.tickets = {};
    }
    //we add the ticket to the session
    //we keep all the open tickets in the session until they are closed
    //req.session.business.tickets.push(ticket);
    req.session.business.tickets[req.body.table] = new_ticket;
    //console.log(req.session.business.tickets);
    res.json(new_ticket); */
});

router.post('/add_item_to_ticket', function(req,res,next) {
    //get the right table in the session
    //check if item exists then increase the amount and ticket price
    //if it doesnt exist then insert the item in the items of ticket
    //if (req.session.business.tickets[req.body.table].items.id)
    var itemExists = false;
    req.session.business.tickets[req.body.table].items.forEach(function(item,index,array) {
        if (item.id == req.body.id) {
            itemExists = true;
            item.amount += 1;
            req.session.business.tickets[req.body.table].total_price += req.body.price;
            //break;
        }
    });
    //if the item didnt exist we append it to the items array
    if (itemExists == false) {
        var item = {id:req.body.id,name:req.body.name,amount:1,price:req.body.price,checked:true};
        req.session.business.tickets[req.body.table].items.push(item);
        req.session.business.tickets[req.body.table].total_price += req.body.price;
    }
    console.log(req.session.business.tickets);
    res.json(req.body);
});

router.post('/remove_item', function(req,res,next) {
    req.session.business.tickets[req.body.table].items.forEach(function(item,index,array) {
        if (item.id == req.body.item_id) {
            if (item.amount == 1) {
                req.session.business.tickets[req.body.table].items.splice(index, 1);
            }
            else {
                item.amount -= 1;
            }
            req.session.business.tickets[req.body.table].total_price -= req.body.item_price;
        }
    });

    res.json(req.session.business.tickets[req.body.table]);
});

//it is called when we want to close the table to retrieve the amount
//of money that the client needs to pay
router.post('/get_table_sum', function(req,res,next) {
    res.json(req.session.business.tickets[req.body.table_nr].total_price);
});

//called when we pay the ticket
router.post('/close_table', function(req,res,next) {
    //NEW IMPLEMENTATION
    var ticket = req.session.business.tickets[req.body.table_nr];
    var shouldCloseTicket = true;
    var itemsToClose = [];
    var response = {};
    var currentlyPaying = 0;
    response.status = 'closed';
    response.table_nr = req.body.table_nr;
    for (var i = ticket.items.length - 1; i >= 0; i--) {
        if (ticket.items[i].checked == true) {
            //remove it from the session
            itemsToClose.push(ticket.items[i]);
            currentlyPaying += ticket.items[i].amount * ticket.items[i].price;
            ticket.total_price -= ticket.items[i].amount * ticket.items[i].price;
            ticket.items.splice(i, 1);
        }
        else {
            shouldCloseTicket = false;
            response.status = 'open';
        }
    }

    //insert the payed items in the databse
    E_Tickets.updateItemsStatus(req.session.business.tickets[req.body.table_nr].db_id,itemsToClose);
    //we have to loop through all the items payed and close their status to 1

    if (shouldCloseTicket == true) {
        //then we should update the ticket to closed
        E_Tickets.closeTicket(req.session.business.tickets[req.body.table_nr].db_id);
    }

    response.items = req.session.business.tickets[req.body.table_nr].items;
    response.total_price = req.session.business.tickets[req.body.table_nr].total_price;

    //set the waiter counter for tables served
    req.session.business.workers.forEach(function(worker,index,array) {
        if (worker.id == ticket.waiter_id) {
            if (response.status == 'closed') {
                if (worker.tables_served == undefined) {
                    worker.tables_served = 1;
                }
                else {
                    worker.tables_served += 1;
                }
            }
            if (worker.profit == undefined) {
                worker.profit = currentlyPaying;
            }
            else {
                worker.profit += currentlyPaying;
            }
            console.log('this is current paying ::'+currentlyPaying);
        }
    });

    res.json(response);
   /*
    ticket.items.forEach(function(item,index,array) {
        console.log('item :' + item.name + ' checked :'+item.checked);
        if (item.checked) {
            //remove it from the session
            itemsToClose.push(item);
            ticket.total_price -= item.amount * item.price;
            ticket.items.splice(index, 1);
        }
        else {
            console.log('NOO::' + item.name + ' IS NOT CHECKED');
        }
    });*/
/*
    console.log('the count is :::'+ req.session.business.tickets[req.body.table_nr].items.length);
    console.log('after.....................................');
    console.log(req.session.business.tickets[req.body.table_nr]);
    console.log('after.....................................');

    console.log('to close ...................................');
    console.log(itemsToClose);
    console.log('to close ...................................'); */

    //if we should close the table then we do it in the db
    //we also change the status of the items in the db
    //we return the remaining ticket with a status saying closed, open
    /*
    var ticket = req.session.business.tickets[req.body.table_nr];
    var ticketWithCheckedItemsOnly = E_Tickets.getItemsChecked(ticket.items);
    //if there are some ticket items still dont close the table but remove these
    //checked ones from the session
    var lengthOfAllItems = req.session.business.tickets[req.body.table_nr].items.length;
    var lengthOfCheckedItems = ticketWithCheckedItemsOnly.length;

    var response = {};

    if (lengthOfCheckedItems < lengthOfAllItems) {
        //it means we still have some tickets unprinted
        //we shouldnt close the ticket yet we should remove
        //the checked items from the session
        //send a request to the database and update the items status to 1 i.e. payed
        response.status = 'open';
        var storePayedItems = new Promise(function(resolve,reject) {
            var response = E_Tickets.saveTicketRows(ticket.id,ticketWithCheckedItemsOnly);
            return resolve(storePayedItems);
        });

        storePayedItems.then(function(response) {
            if (response.status == 'success') {
                console.log('it was damn successful');
                req.session.business.tickets[req.body.table_nr].items.foreach(function(item,index,array) {
                    //we have to remove it from the session
                    if (item.)
                });
            }
            else {
                console.log('something went wrong');
            }
        });
    }
    else {
        //we are about to pay all .. we should close the ticket
        // and remove the table from the session
        response.status = 'closed';

    } */
    /*
        IN ORDER To close a table first we have to check if we
        have ticket items if we do then we have to close the checked ones
        i.e. remove them from the session
        if there is no more checked items we mark the ticket as closed too
        and return approperiate message
        if there are still ticket items unprinted we just
    */
    /*
    //ACTIONS : 1 store the ticket 2 store ticket items(rows), update workers stats
    var ticket = req.session.business.tickets[req.body.table_nr];
    var business_id = req.session.business.business.id;
    var ticketWithCheckedItemsOnly = E_Tickets.getItemsChecked(ticket.items);
    //STEPS : GET ONLY THE ITEMS THAT ARE CHECKED IF ALL CHECKED
    //WE SHOULD REMOVE IT FROM THE
    var storeTicket = new Promise(function(resolve,reject) {
        if (req.session.business.tickets[req.body.table_nr]['ticket_id'] == undefined) {
            var response = E_Tickets.saveTicket(business_id,ticket);
            return resolve(response);
        }
        else {
            //if the ticket_id is defined it means we have it in the database once
            //we have to also update the ticket sum here
            return resolve(req.session.business.tickets[req.body.table_nr]['ticket_id']);
        }
    });
    //store the ticket rows too
    storeTicket.then(function(response) {
        E_Tickets.saveTicketRows(response.ticket_id,/*ticket.items*///ticketWithCheckedItemsOnly);
    /*    if (E_Tickets.hasUncheckedItems) {
            //if there are unchecked items then we dont want to remove the entire ticket
            //we only set the ticket to the uncheckedItems
            req.session.business.tickets[req.body.table_nr].items = E_Tickets.uncheckedItems;
            //we have to add the response ticket id to that current ticket
            req.session.business.tickets[req.body.table_nr]['ticket_id'] = response.ticket_id;
            //updating the total price
            req.session.business.tickets[req.body.table_nr].total_price = E_Tickets.totalPriceOfUnCheckedItems();
        }
        else {
            //delete the ticket from the session
            delete req.session.business.tickets[req.body.table_nr];
        }

        //set the waiter counter for tables served
        req.session.business.workers.forEach(function(worker,index,array) {
            if (worker.id == ticket.waiter_id) {
                if (worker.tables_served == undefined) {
                    worker.tables_served = 1;
                }
                else {
                    worker.tables_served += 1;
                }
                if (worker.profit == undefined) {
                    worker.profit = ticket.total_price;
                }
                else {
                    worker.profit += ticket.total_price;
                }
            }
        });

        res.json(req.session.business.tickets[req.body.table_nr]);
    }); */
});

router.post('/change_pin', function(req,res,next) {
    var ifPinExist = new Promise(function(resolve,reject) {
        var response = E_Workers.pinExists(req.body);
        resolve(response);
    });
    ifPinExist.then(function(response) {
        if (response.status == 'exist') {
            res.json(response);
        }
        else {
            //if the pin is not taken then update it
            var updatePin = new Promise(function(resolve,reject) {
                var response = E_Workers.updateWorkerPin(req.body);
                resolve(response);
            });
            updatePin.then(function(response) {
                //we have to change the password from the session too
                req.session.business.workers.forEach(function(worker,index,array) {
                    if (worker.id == req.body.waiter_id) {
                        worker.pin = req.body.new_pin;
                    }
                });
                //return the final response
                res.json(response);
            }).catch(function(err) {
                res.json(err);
            });
        }
    });
});

router.post('/remove_ticket', function(req,res,next) {
    //remove the current ticket from the session if there is a status message
    //why we remove the ticket then store it in the database
    if (req.session.business.tickets[req.body.table_nr].items.length > 0) {
        //there are items we should set the status message to canceled true
        var cancelTicket = new Promise(function(resolve,reject) {
            var business_id = req.session.business.business.id;
            var ticket = req.session.business.tickets[req.body.table_nr];
            var message = req.body.message;
            var response = E_Tickets.cancelTicket(business_id,ticket,message);
            resolve(response);
        });

        cancelTicket.then(function(response) {
            //it returns the ticket id in the database we might use it
            res.json({status:'success'});
        });
    }
    else {
        //just remove it fromt he session
        delete req.session.business.tickets[req.body.table_nr];
        res.json({status:'success'});
    }
});

router.post('/toggle_item_checked',function(req,res,next) {
    req.session.business.tickets[req.body.table_nr].items.forEach(function(item,index,ticket) {
        if (item.id == req.body.item_id) {
            item.checked = !item.checked;
            console.log('here');
            console.log(req.session.business.tickets[req.body.table_nr]);
            res.json({status:'success',item_id:req.body.item_id});
        }
    });

});

router.post('/all_tickets_from_date', function(req,res,next) {
    var businessID = req.session.business.business.id;
    var ticketsForDate = new Promise(function(resolve,reject) {
        var response = E_Tickets.getTicketForDate(req.body.date,businessID);
        resolve(response);
    });

    ticketsForDate.then(function(response){

    });
});

// export your router
module.exports = router;
